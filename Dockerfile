FROM microsoft/aspnetcore:2.0 AS base
WORKDIR /app

FROM microsoft/aspnetcore-build:2.0 AS build
WORKDIR /src
#COPY *.sln ./
COPY Common/ ./Common
COPY Server/CheatBuster.Server/ ./Server/CheatBuster.Server/
RUN dotnet restore Server/CheatBuster.Server/*.csproj
RUN dotnet build Server/CheatBuster.Server/*.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish Server/CheatBuster.Server/*.csproj -c Release -o /app/out

FROM base AS final
WORKDIR /app
COPY --from=publish /app/out .
ENTRYPOINT ["dotnet", "CheatBuster.Server.dll"]

EXPOSE 8080
