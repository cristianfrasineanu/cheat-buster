﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace CheatBuster.Server.LiveStatus
{
    internal class ConnectionManager
    {
        private readonly ConcurrentDictionary<MachineIdentity, WebSocket> sockets = new ConcurrentDictionary<MachineIdentity, WebSocket>();

        public ConcurrentDictionary<MachineIdentity, WebSocket> GetAll() => sockets;

        public IEnumerable<WebSocket> GetSocketsByRole(string role) => sockets.Where(p => p.Key.Role == role).Select(p => p.Value);

        public WebSocket GetSocketByUserName(string userName) => sockets.FirstOrDefault(p => p.Key.UserName == userName).Value;

        public MachineIdentity GetSocketIdentity(WebSocket socket) => sockets.FirstOrDefault(p => p.Value == socket).Key;

        public void AddSocket(MachineIdentity identity, WebSocket socket) => sockets.TryAdd(identity, socket);

        public async Task RemoveSocket(MachineIdentity identity)
        {
            sockets.TryRemove(identity, out var socket);
            try
            {
                await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closed by the connection manager", CancellationToken.None);
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}