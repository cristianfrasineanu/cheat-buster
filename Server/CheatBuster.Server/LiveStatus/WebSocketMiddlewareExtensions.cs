﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace CheatBuster.Server.LiveStatus
{
    internal static class WebSocketMiddlewareExtensions
    {
        public static IServiceCollection AddWebSocketManager(this IServiceCollection services)
        {
            services.AddTransient<ConnectionManager>();
            services.AddScoped<MachineStatusHandler>();

            return services;
        }

        public static IApplicationBuilder MapWebSocketManager(this IApplicationBuilder app, PathString path, WebSocketHandler handler) =>
            app.Map(path, filteredPipeline => filteredPipeline.UseMiddleware<WebSocketMiddleware>(handler));
    }
}