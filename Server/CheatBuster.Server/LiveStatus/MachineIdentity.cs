﻿namespace CheatBuster.Server.LiveStatus
{
    internal class MachineIdentity
    {
        public string UserName { get; set; }
        public string Role { get; set; }
        public string JobCluster { get; set; }
    }
}