﻿using System;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using CheatBuster.Server.Client.Machine;
using CheatBuster.Server.Common.Authentication;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

namespace CheatBuster.Server.LiveStatus
{
    internal class WebSocketMiddleware
    {
        private readonly RequestDelegate next;
        private readonly ILogger<WebSocketMiddleware> logger;
        private WebSocketHandler WebSocketHandler { get; }

        public WebSocketMiddleware(RequestDelegate next, WebSocketHandler webSocketHandler, ILogger<WebSocketMiddleware> logger)
        {
            this.next = next;
            WebSocketHandler = webSocketHandler;
            this.logger = logger;
        }

        public async Task Invoke(HttpContext context, UserManager<IdentityUser> userManager, IMachinesRepository machinesRepository)
        {
            if (!context.WebSockets.IsWebSocketRequest)
                return;

            var socket = await context.WebSockets.AcceptWebSocketAsync();
            var authResult = await context.AuthenticateAsync(JwtBearerDefaults.AuthenticationScheme);
            if (!authResult.Succeeded)
            {
                logger.LogError("WebSocket client authentication failed.");
                try
                {
                    await socket.CloseOutputAsync(WebSocketCloseStatus.EndpointUnavailable, "Invalid authorization token.", CancellationToken.None);
                }
                catch (Exception)
                {
                    //
                }

                return;
            }

            var userName = authResult.Principal.FindFirst("unique_name").Value;
            var role = (await userManager.GetRolesAsync(await userManager.FindByNameAsync(userName))).First();

            string cluster = null;
            if (role == UserRoles.MonitoredMachine)
                cluster = machinesRepository.LoadMachineJobClusterName(userName);
            await WebSocketHandler.OnConnected(new MachineIdentity {UserName = userName, Role = role, JobCluster = cluster}, socket);

            await Receive(socket, async (result, buffer) =>
            {
                switch (result.MessageType)
                {
                    case WebSocketMessageType.Text:
                        await WebSocketHandler.Receive(socket, result, buffer);
                        break;
                    case WebSocketMessageType.Close:
                        await WebSocketHandler.OnDisconnected(socket);
                        break;
                    case WebSocketMessageType.Binary:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            });

            // TODO - investigate the Kestrel exception thrown when this is the last middleware
            // await next.Invoke(context);
        }

        private async Task Receive(WebSocket socket, Action<WebSocketReceiveResult, byte[]> handleMessage)
        {
            var buffer = new byte[1024 * 4];

            while (socket.State == WebSocketState.Open)
            {
                try
                {
                    var result = await socket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                    handleMessage(result, buffer);
                }
                catch (Exception e)
                {
                    logger.LogError(e.Message);
                    await WebSocketHandler.OnDisconnected(socket);
                    break;
                }
            }
        }
    }
}