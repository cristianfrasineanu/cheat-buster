﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CheatBuster.Server.LiveStatus
{
    internal abstract class WebSocketHandler
    {
        protected ConnectionManager ConnectionManager { get; set; }

        protected WebSocketHandler(ConnectionManager connectionManager)
        {
            ConnectionManager = connectionManager;
        }

        public virtual async Task OnConnected(MachineIdentity identity, WebSocket socket)
        {
            ConnectionManager.AddSocket(identity, socket);
        }

        public virtual async Task OnDisconnected(WebSocket socket)
        {
            await ConnectionManager.RemoveSocket(ConnectionManager.GetSocketIdentity(socket));
        }

        public async Task SendMessage(WebSocket socket, string message)
        {
            if (socket == null || socket.State != WebSocketState.Open)
                return;

            await socket.SendAsync(
                new ArraySegment<byte>(Encoding.ASCII.GetBytes(message), 0, message.Length),
                WebSocketMessageType.Text,
                true,
                CancellationToken.None
            );
        }

        public async Task SendMessage(MachineIdentity identity, string message)
        {
            await SendMessage(ConnectionManager.GetSocketByUserName(identity.UserName), message);
        }

        public async Task SendMessageToRole(string role, string message)
        {
            foreach (var socket in ConnectionManager.GetSocketsByRole(role))
                if (socket.State == WebSocketState.Open)
                    await SendMessage(socket, message);
        }

        public async Task SendMessageToAll(string message)
        {
            foreach (var pair in ConnectionManager.GetAll())
                if (pair.Value.State == WebSocketState.Open)
                    await SendMessage(pair.Value, message);
        }

        // Allows each handler to implements its own response parsing logic.
        public abstract Task Receive(
            WebSocket socket,
            WebSocketReceiveResult result,
            byte[] buffer
        );
    }
}