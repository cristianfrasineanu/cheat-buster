﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CheatBuster.Common.Models.WebSocket.Messages;
using CheatBuster.Server.Client.Machine.Model;
using CheatBuster.Server.Common.Authentication;
using Microsoft.Extensions.Logging;

namespace CheatBuster.Server.LiveStatus
{
    internal class MachineStatusHandler : WebSocketHandler
    {
        private static readonly ICollection<MachineStatus> Machines = new List<MachineStatus>();
        private static readonly ICollection<string> LockedMachineNames = new List<string>();
        private readonly ILogger<MachineStatusHandler> logger;

        public MachineStatusHandler(ConnectionManager connectionManager, ILogger<MachineStatusHandler> logger) : base(connectionManager)
        {
            this.logger = logger;
        }

        public override async Task OnConnected(MachineIdentity identity, WebSocket socket)
        {
            await base.OnConnected(identity, socket);

            await SendMessageToRole(UserRoles.Supervisor, AdminMessageFormatter.GenerateInformationMessage($"{identity.Role} ({identity.UserName}) connected."));
            if (identity.Role == UserRoles.Supervisor)
            {
                await SendMessage(identity, AdminMessageFormatter.GenerateAddMessage(Machines));
            }
            else if (identity.Role == UserRoles.MonitoredMachine)
            {
                var machineStatus = new MachineStatus
                {
                    MachineName = identity.UserName,
                    IsActive = false,
                    IsLocked = false,
                    JobCluster = identity.JobCluster
                };
                Machines.Add(machineStatus);
                await SendMessageToRole(UserRoles.Supervisor, AdminMessageFormatter.GenerateAddMessage(machineStatus));
                if (LockedMachineNames.Contains(machineStatus.MachineName))
                    await SendMessage(socket, ClientMessageFormatter.GenerateLockedMessage(""));
            }
        }

        public override async Task OnDisconnected(WebSocket socket)
        {
            var identity = ConnectionManager.GetSocketIdentity(socket);
            if (identity == null) return;
            await base.OnDisconnected(socket);

            await SendMessageToRole(UserRoles.Supervisor, AdminMessageFormatter.GenerateInformationMessage($"{identity.Role} ({identity.UserName}) disconnected."));
            if (identity.Role == UserRoles.MonitoredMachine)
            {
                var machineToRemove = Machines.First(machine => machine.MachineName == identity.UserName);
                Machines.Remove(machineToRemove);
                await SendMessageToRole(UserRoles.Supervisor, AdminMessageFormatter.GenerateDeleteMessage(machineToRemove));
            }
        }

        public override async Task Receive(WebSocket socket, WebSocketReceiveResult result, byte[] buffer)
        {
            var socketUser = ConnectionManager.GetSocketIdentity(socket).UserName;
            var payload = Encoding.UTF8.GetString(buffer, 0, result.Count);

            var clientSockets = ConnectionManager.GetSocketsByRole(UserRoles.MonitoredMachine);
            var adminSockets = ConnectionManager.GetSocketsByRole(UserRoles.Supervisor);
            try
            {
                if (clientSockets.Contains(socket)) await HandleClientMessage(socketUser, payload);
                else if (adminSockets.Contains(socket)) await HandleAdminMessage(socketUser, payload);
            }
            catch (Exception e)
            {
                logger.LogError(e.Message);
            }
        }

        private async Task HandleClientMessage(string user, string payload)
        {
            MachineStatus status;

            var message = MessageFormatter.DeserializeMessage<string>(payload);
            if (message.Action == ClientMessages.ServiceActivated)
            {
                status = Machines.First(machine => machine.MachineName == user);
                status.IsActive = true;
                logger.LogInformation("A client was activated.");
            }
            else if (message.Action == ClientMessages.ServiceDeactivated)
            {
                status = Machines.First(machine => machine.MachineName == user);
                status.IsActive = false;
                logger.LogInformation("A client was deactivated.");
            }
            else if (message.Action == ClientMessages.Locked)
            {
                status = Machines.First(machine => machine.MachineName == user);
                status.IsLocked = true;
                if (!LockedMachineNames.Contains(status.MachineName))
                    LockedMachineNames.Add(status.MachineName);
                logger.LogInformation("A client was locked.");
            }
            else if (message.Action == ClientMessages.Unlocked)
            {
                status = Machines.First(machine => machine.MachineName == user);
                status.IsLocked = false;
                LockedMachineNames.Remove(status.MachineName);
                logger.LogInformation("A client was unlocked.");
            }
            else
            {
                logger.LogError($"Unknown client message action: {message.Action}");
                return;
            }

            if (!string.IsNullOrEmpty(message.Data)) logger.LogInformation($"Client message data: {message.Data}");

            await SendMessageToRole(UserRoles.Supervisor, AdminMessageFormatter.GenerateUpdateMessage(status));
        }

        private async Task HandleAdminMessage(string user, string payload)
        {
            WebSocket targetSocket = null;

            var message = MessageFormatter.DeserializeMessage<string>(payload);
            if (message.Action != AdminMessages.Info)
                targetSocket = ConnectionManager.GetSocketByUserName(message.Data);

            if (message.Action == AdminMessages.Info) logger.LogInformation(message.Data);
            else if (message.Action == AdminMessages.ManualLock)
                await SendMessage(targetSocket, ClientMessageFormatter.GenerateLockedMessage(""));
            else if (message.Action == AdminMessages.ManualUnlock)
                await SendMessage(targetSocket, ClientMessageFormatter.GenerateUnlockedMessage(""));
            else if (message.Action == AdminMessages.Terminate && targetSocket != null)
                await targetSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Disconnected by the supervisor", CancellationToken.None);
            else logger.LogError($"Unknown admin message action: {message.Action}");
        }
    }
}