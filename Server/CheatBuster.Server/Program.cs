﻿using System;
using CheatBuster.Server.Client.Monitoring;
using CheatBuster.Server.Common.Authentication;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CheatBuster.Server
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            using (var scope = host.Services.CreateScope())
            {
                var scopeServiceProvider = scope.ServiceProvider;
                SeedAuthenticationDatabase(scopeServiceProvider);
                SeedMonitoringDatabase(scopeServiceProvider);
            }

            host.Run();
        }

        private static void SeedAuthenticationDatabase(IServiceProvider scopeServiceProvider)
        {
            var logger = scopeServiceProvider.GetRequiredService<ILogger<Program>>();

            try
            {
                var authenticationContext = scopeServiceProvider.GetRequiredService<AuthenticationContext>();
                authenticationContext.Database.Migrate();
                UsersSeeder.Initialize(scopeServiceProvider).Wait();
                logger.LogInformation("Successfully seeded the authentication database.");
            }
            catch (Exception e)
            {
                logger.LogError(e, "Unexpected error when seeding the authentication database.");
            }
        }

        private static void SeedMonitoringDatabase(IServiceProvider scopeServiceProvider)
        {
            var logger = scopeServiceProvider.GetRequiredService<ILogger<Program>>();

            try
            {
                var monitoringContext = scopeServiceProvider.GetRequiredService<MonitoringContext>();
                monitoringContext.Database.Migrate();
                JobsSeeder.Initialize(scopeServiceProvider);
                logger.LogInformation("Successfully seeded the client monitoring database.");
            }
            catch (Exception e)
            {
                logger.LogError(e, "Unexpected error when seeding the client monitoring database.");
            }
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls("http://*:8080")
                .UseStartup<Startup>()
                .Build();
    }
}