﻿namespace CheatBuster.Server.Client.Machine.Model
{
    internal class MachineStatus
    {
        public string MachineName { get; set; }
        public string JobCluster { get; set; }
        public bool IsActive { get; set; }
        public bool IsLocked { get; set; }
    }
}