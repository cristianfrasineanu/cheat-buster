﻿using System.Collections.Generic;

namespace CheatBuster.Server.Client.Machine.Model
{
    public class MachineDetails
    {
        public int Id { get; set; }
        public string MachineName { get; set; }
        public int JobId { get; set; }
        public ICollection<Screenshot> Screenshots { get; set; }
    }
}