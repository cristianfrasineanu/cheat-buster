﻿using CheatBuster.Common.Models;

namespace CheatBuster.Server.Client.Machine.Model
{
    public class Screenshot : Image
    {
        public int Id { get; set; }
        public int MachineId { get; set; }
        public new byte[] Data { get; set; }
    }
}