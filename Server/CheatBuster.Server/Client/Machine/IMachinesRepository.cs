﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CheatBuster.Common.Models;
using CheatBuster.Server.Client.Machine.Model;
using Microsoft.AspNetCore.Identity;

namespace CheatBuster.Server.Client.Machine
{
    public interface IMachinesRepository : IDisposable
    {
        Task<IEnumerable<IdentityUser>> LoadMachines();
        string LoadMachineJobClusterName(string name);
        IEnumerable<Screenshot> LoadScreenshots(string machineName);
        bool AddScreenshot(string machineName, Image image);
        int Save();
    }
}