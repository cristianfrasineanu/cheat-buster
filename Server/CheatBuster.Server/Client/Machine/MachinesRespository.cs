﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CheatBuster.Common.Models;
using CheatBuster.Server.Client.Machine.Model;
using CheatBuster.Server.Client.Monitoring;
using CheatBuster.Server.Common.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace CheatBuster.Server.Client.Machine
{
    public class MachinesRespository : IMachinesRepository
    {
        private readonly MonitoringContext context;
        private readonly UserManager<IdentityUser> userManager;
        private readonly ILogger<MachinesRespository> logger;

        public MachinesRespository(MonitoringContext context, UserManager<IdentityUser> userManager, ILogger<MachinesRespository> logger)
        {
            this.context = context;
            this.userManager = userManager;
            this.logger = logger;
        }

        public async Task<IEnumerable<IdentityUser>> LoadMachines() => await userManager.GetUsersInRoleAsync(UserRoles.MonitoredMachine);

        public string LoadMachineJobClusterName(string machineName)
        {
            var machineJob = context.Jobs.FirstOrDefault(job => job.Machines.FirstOrDefault(machine => machine.MachineName == machineName) != null);
            return machineJob != null ? machineJob.AccordingCluster : "None";
        }

        public IEnumerable<Screenshot> LoadScreenshots(string machineName)
        {
            var machineToFind = context.Machines
                .Include(machine => machine.Screenshots)
                .FirstOrDefault(machine => machine.MachineName == machineName);
            return machineToFind?.Screenshots;
        }

        public bool AddScreenshot(string machineName, Image image)
        {
            var machineToFind = context.Machines.FirstOrDefault(machine => machine.MachineName == machineName);
            if (machineToFind == null) return false;
            if (machineToFind.Screenshots == null) machineToFind.Screenshots = new List<Screenshot>();
            machineToFind.Screenshots.Add(new Screenshot
            {
                FileName = image.FileName,
                MimeType = image.MimeType,
                Data = image.Data
            });
            return Save() > 0;
        }

        public int Save() => context.SaveChanges();

        void IDisposable.Dispose() => context.Dispose();
    }
}