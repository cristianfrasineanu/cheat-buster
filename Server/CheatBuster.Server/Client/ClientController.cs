﻿using System.Linq;
using System.Threading.Tasks;
using CheatBuster.Common.Models;
using CheatBuster.Server.Client.Machine;
using CheatBuster.Server.Client.Machine.Model;
using CheatBuster.Server.Client.Monitoring;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CheatBuster.Server.Client
{
    [Route("[controller]")]
    public class ClientController : Controller
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly IJobsRepository jobsRepository;
        private readonly IMachinesRepository machinesRepository;
        private readonly ILogger<ClientController> logger;

        public ClientController(UserManager<IdentityUser> userManager, IMachinesRepository machinesRepository, IJobsRepository jobsRepository, ILogger<ClientController> logger)
        {
            this.userManager = userManager;
            this.machinesRepository = machinesRepository;
            this.jobsRepository = jobsRepository;
            this.logger = logger;
        }

        [HttpGet("job")]
        [Authorize(Roles = "MonitoredMachine")]
        public IActionResult GetJob()
        {
            var machineName = User.FindFirst("unique_name")?.Value;
            var job = jobsRepository.LoadMachineJob(machineName);
            return new JsonResult(job);
        }

        [HttpPost("screenshot")]
        [Authorize(Roles = "MonitoredMachine")]
        public IActionResult PublishScreenshot([FromBody] Image image)
        {
            var machineName = User.FindFirst("unique_name")?.Value;
            if (!machinesRepository.AddScreenshot(machineName, image))
                return new BadRequestResult();
            return new OkResult();
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterMachine([FromBody] MachineAuthentication model)
        {
            var user = new IdentityUser
            {
                UserName = model.MachineName,
                Email = null
            };
            var result = await userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded) return new BadRequestObjectResult(result.Errors);

            var createdUser = await userManager.FindByNameAsync(user.UserName);
            var userMachine = new MachineDetails {MachineName = createdUser.NormalizedUserName};
            await userManager.AddToRoleAsync(createdUser, "MonitoredMachine");
            jobsRepository.LoadJobs().First(job => job.AccordingCluster == "Default").Machines.Add(userMachine);
            jobsRepository.Save();

            return new OkResult();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                jobsRepository.Dispose();
                machinesRepository.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}