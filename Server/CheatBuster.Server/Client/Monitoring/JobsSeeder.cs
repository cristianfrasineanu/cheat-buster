﻿using System;
using System.Collections.Generic;
using System.Linq;
using CheatBuster.Common.Models.Monitoring;
using CheatBuster.Server.Client.Machine.Model;
using CheatBuster.Server.Client.Monitoring.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CheatBuster.Server.Client.Monitoring
{
    internal class JobsSeeder
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var jobsContext =
                new MonitoringContext(serviceProvider.GetRequiredService<DbContextOptions<MonitoringContext>>()))
            {
                if (jobsContext.Jobs.Any())
                    return;

                jobsContext.Jobs.Add(
                    new MonitoringJob
                    {
                        AccordingCluster = "Default",
                        Rules = new List<Rule>
                        {
                            new Rule
                            {
                                Type = RuleType.Blacklist,
                                Action = ActionType.SystemEvent,
                                Filter = "USB_Storage_Device"
                            },
                            new Rule
                            {
                                Type = RuleType.Blacklist,
                                Action = ActionType.WebRequest,
                                Filter = @".*\.ase\.ro.*"
                            },
                            new Rule
                            {
                                Type = RuleType.Blacklist,
                                Action = ActionType.SystemEvent,
                                Filter = "Desktop_Activity"
                            },
                            new Rule
                            {
                                Type = RuleType.Blacklist,
                                Action = ActionType.SystemProcess,
                                Filter = "Internet_Browsing"
                            },
                            new Rule
                            {
                                Type = RuleType.Blacklist,
                                Action = ActionType.SystemProcess,
                                Filter = "Document_Management"
                            },
                        },
                        Machines = new List<MachineDetails>
                        {
                            new MachineDetails {MachineName = "FOO01"},
                            new MachineDetails {MachineName = "FOO02"}
                        },
                        StartAt = DateTime.Now,
                        EndAt = DateTime.Now + TimeSpan.FromHours(3),
                        PollingRate = TimeSpan.FromMinutes(1),
                        PushRate = null
                    }
                );
                jobsContext.SaveChanges();
            }
        }
    }
}