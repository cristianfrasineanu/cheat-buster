﻿using CheatBuster.Common.Models.Monitoring;

namespace CheatBuster.Server.Client.Monitoring.Model
{
    public class Rule : RuleBase
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public new ActionType Action { get; set; }
    }
}