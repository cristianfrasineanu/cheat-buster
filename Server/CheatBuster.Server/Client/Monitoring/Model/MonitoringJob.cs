﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CheatBuster.Common.Models.Monitoring;
using CheatBuster.Server.Client.Machine.Model;

namespace CheatBuster.Server.Client.Monitoring.Model
{
    public class MonitoringJob : MonitoringJobBase<Rule>
    {
        public int Id { get; set; }
        [StringLength(10)] public override string AccordingCluster { get; set; }
        public override ICollection<Rule> Rules { get; set; }
        public ICollection<MachineDetails> Machines { get; set; }
    }
}