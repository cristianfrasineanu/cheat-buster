﻿using System;
using System.Collections.Generic;
using CheatBuster.Server.Client.Monitoring.Model;

namespace CheatBuster.Server.Client.Monitoring
{
    public interface IJobsRepository : IDisposable
    {
        IEnumerable<MonitoringJob> LoadJobs();
        MonitoringJob LoadJobById(int jobId);
        MonitoringJob LoadMachineJob(string machineName);
        int CreateJob(MonitoringJob job);
        bool DeleteJob(int jobId);
        bool UpdateJob(MonitoringJob newJob);
        int Save();
    }
}