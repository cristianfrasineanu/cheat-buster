﻿using System;
using System.Linq;
using CheatBuster.Server.Client.Machine.Model;
using CheatBuster.Server.Client.Monitoring.Model;
using Microsoft.EntityFrameworkCore;

namespace CheatBuster.Server.Client.Monitoring
{
    public class MonitoringContext : DbContext
    {
        public DbSet<MonitoringJob> Jobs { get; set; }
        public DbSet<MachineDetails> Machines { get; set; }
        public DbSet<Rule> Rules { get; set; }

        public MonitoringContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MonitoringJob>()
                .ToTable("Job")
                .HasIndex(job => job.AccordingCluster)
                .IsUnique();
            modelBuilder.Entity<MonitoringJob>()
                .HasMany(job => job.Rules)
                .WithOne()
                .HasForeignKey(rule => rule.JobId);
            modelBuilder.Entity<Rule>()
                .ToTable("Rule");
            modelBuilder.Entity<MonitoringJob>()
                .HasMany(job => job.Machines)
                .WithOne()
                .HasForeignKey(machine => machine.JobId);
            modelBuilder.Entity<MachineDetails>()
                .ToTable("Machine")
                .HasIndex(machine => machine.MachineName)
                .IsUnique();
            modelBuilder.Entity<MachineDetails>()
                .HasMany(machine => machine.Screenshots)
                .WithOne()
                .HasForeignKey(screenshot => screenshot.MachineId);
        }


        public override int SaveChanges()
        {
            AddTimestamp();
            return base.SaveChanges();
        }

        private void AddTimestamp()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is MonitoringJob
                                                              && (x.State == EntityState.Added || x.State == EntityState.Modified));
            foreach (var entity in entities) ((MonitoringJob) entity.Entity).LastModified = DateTime.Now;
        }
    }
}