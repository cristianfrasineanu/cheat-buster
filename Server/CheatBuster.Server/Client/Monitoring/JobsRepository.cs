﻿using System;
using System.Collections.Generic;
using System.Linq;
using CheatBuster.Server.Client.Monitoring.Model;
using Microsoft.EntityFrameworkCore;

namespace CheatBuster.Server.Client.Monitoring
{
    internal class JobsRepository : IJobsRepository
    {
        private readonly MonitoringContext context;

        public JobsRepository(MonitoringContext context) => this.context = context;

        public IEnumerable<MonitoringJob> LoadJobs()
        {
            return context.Jobs
                .Include(job => job.Rules)
                .Include(job => job.Machines)
                .ToList();
        }

        public MonitoringJob LoadJobById(int jobId)
        {
            return context.Jobs
                .Include(job => job.Rules)
                .Include(job => job.Machines)
                .FirstOrDefault(c => c.Id == jobId);
        }

        public MonitoringJob LoadMachineJob(string machineName)
        {
            return context.Jobs
                .Include(job => job.Rules)
                .SingleOrDefault(job => job.Machines.Any(m => m.MachineName == machineName));
        }

        public int CreateJob(MonitoringJob job)
        {
            context.Jobs.Add(job);
            if (Save() > 0)
                return job.Id;

            return -1;
        }

        public bool DeleteJob(int jobId)
        {
            var jobToDelete = context.Jobs.Find(jobId);
            if (jobToDelete.AccordingCluster == "Default") return false;

            context.Jobs.Remove(jobToDelete);
            return Save() > 0;
        }

        public bool UpdateJob(MonitoringJob newJob)
        {
            var jobToUpdate = context.Jobs
                .Include(monitoringJob => monitoringJob.Rules)
                .Include(monitoringJob => monitoringJob.Machines)
                .SingleOrDefault(monitoringJob => monitoringJob.Id == newJob.Id);
            if (jobToUpdate == null) return false;

            // Updates only the scalar properties. Have to update the related properties manually. Otherwise, only the base entity gets updated.
            context.Entry(jobToUpdate).CurrentValues.SetValues(newJob);

            UpdateMachinesProperty(newJob, jobToUpdate);
            UpdateRulesProperty(newJob, jobToUpdate);

            return Save() > 0;
        }

        private void UpdateMachinesProperty(MonitoringJob newJob, MonitoringJob jobToUpdate)
        {
            if (newJob.Machines == null) return;

            foreach (var machine in jobToUpdate.Machines)
                if (newJob.Machines.All(newMachine => newMachine.MachineName != machine.MachineName))
                    context.Machines.Remove(machine);
            foreach (var newMachine in newJob.Machines)
            {
                var existingMachine = jobToUpdate.Machines.SingleOrDefault(machine => machine.MachineName == newMachine.MachineName);
                // A machine can only be added or removed by a supervisor. So we do not care about the existing ones.
                if (existingMachine == null)
                    jobToUpdate.Machines.Add(newMachine);
            }
        }

        private void UpdateRulesProperty(MonitoringJob newJob, MonitoringJob jobToUpdate)
        {
            if (newJob.Rules == null) return;

            foreach (var rule in jobToUpdate.Rules)
                if (newJob.Rules.All(newRule => newRule.Id != rule.Id))
                    context.Rules.Remove(rule);
            foreach (var newRule in newJob.Rules)
            {
                var existingRule = jobToUpdate.Rules.SingleOrDefault(rule => rule.Id == newRule.Id && rule.Id > 0);
                if (existingRule != null)
                    context.Entry(existingRule).CurrentValues.SetValues(newRule);
                else
                    jobToUpdate.Rules.Add(newRule);
            }
        }

        public int Save() => context.SaveChanges();

        void IDisposable.Dispose() => context.Dispose();
    }
}