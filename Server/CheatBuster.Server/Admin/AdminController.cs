﻿using System.Linq;
using System.Threading.Tasks;
using CheatBuster.Server.Client.Machine;
using CheatBuster.Server.Client.Monitoring;
using CheatBuster.Server.Client.Monitoring.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CheatBuster.Server.Admin
{
    [Route("[controller]")]
    public class AdminController : Controller
    {
        private readonly IJobsRepository jobsRepository;
        private readonly IMachinesRepository machinesRepository;
        private readonly ILogger<AdminController> logger;

        public AdminController(IJobsRepository jobsRepository, IMachinesRepository machinesRepository, ILogger<AdminController> logger)
        {
            this.jobsRepository = jobsRepository;
            this.machinesRepository = machinesRepository;
            this.logger = logger;
        }

        [HttpGet("jobs")]
        [Authorize(Roles = "Supervisor")]
        public IActionResult GetJobs()
        {
            var jobs = jobsRepository.LoadJobs();
            return new OkObjectResult(jobs);
        }

        [HttpPost("jobs")]
        [Authorize(Roles = "Supervisor")]
        public IActionResult CreateJob([FromBody] MonitoringJob job)
        {
            if (!ModelState.IsValid)
                return new BadRequestObjectResult(ModelState.Select(result => result.Value.Errors).Where(errors => errors.Count > 0).ToList());

            var createdId = jobsRepository.CreateJob(job);
            if (createdId < 0)
                return new BadRequestResult();
            return new OkObjectResult(createdId);
        }

        [HttpPut("jobs/{id}")]
        [Authorize(Roles = "Supervisor")]
        public IActionResult UpdateJob(int id, [FromBody] MonitoringJob job)
        {
            if (!ModelState.IsValid)
                return new BadRequestObjectResult(ModelState.Select(result => result.Value.Errors).Where(errors => errors.Count > 0).ToList());

            var updateSuccess = jobsRepository.UpdateJob(job);
            if (!updateSuccess)
                return new BadRequestResult();
            return new OkResult();
        }

        [HttpDelete("jobs/{id}")]
        [Authorize(Roles = "Supervisor")]
        public IActionResult DeleteJob(int id)
        {
            if (!ModelState.IsValid)
                return new BadRequestObjectResult(ModelState.Select(result => result.Value.Errors).Where(errors => errors.Count > 0).ToList());

            var deleteSuccess = jobsRepository.DeleteJob(id);
            if (!deleteSuccess)
                return new BadRequestResult();
            return new OkResult();
        }

        [HttpGet("machines")]
        [Authorize(Roles = "Supervisor")]
        public async Task<IActionResult> GetMachineNames()
        {
            var machineUsers = await machinesRepository.LoadMachines();
            var machineNames = machineUsers.Select(machine => machine.NormalizedUserName);
            return new OkObjectResult(machineNames);
        }

        [HttpGet("screenshots")]
        [Authorize(Roles = "Supervisor")]
        public IActionResult GetScreenshots([FromQuery(Name = "machineName")] string machineName)
        {
            var screenshots = machinesRepository.LoadScreenshots(machineName);
            return new OkObjectResult(screenshots);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                jobsRepository.Dispose();
                machinesRepository.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}