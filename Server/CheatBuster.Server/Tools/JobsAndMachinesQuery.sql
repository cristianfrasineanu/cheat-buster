﻿USE ClientMonitoring
SELECT m.machineName, j.StartAt, j.EndAt, j.PollingRate, j.PushRate, j.AccordingCluster
FROM [dbo].[Job] j
INNER JOIN [dbo].[Machine] m
ON j.Id = m.JobId