﻿USE ClientMonitoring
SELECT j.StartAt, j.EndAt, j.PollingRate, j.PushRate, j.AccordingCluster, r.Filter, r.Type, r.Action
FROM [dbo].[Job] j
INNER JOIN [dbo].[Rule] r
ON j.Id = r.JobId