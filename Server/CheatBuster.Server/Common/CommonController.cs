﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CheatBuster.Common.Models;
using CheatBuster.Server.Common.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace CheatBuster.Server.Common
{
    [Route("/")]
    public class CommonController : Controller
    {
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly UserManager<IdentityUser> userManager;
        private readonly IOptions<SecurityConfiguration> securityOptions;

        public CommonController(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager, IOptions<SecurityConfiguration> securityOptions)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.securityOptions = securityOptions;
        }

        [HttpPost("authenticate")]
        [AllowAnonymous]
        public async Task<IActionResult> Authenticate([FromBody] MachineAuthentication model)
        {
            var loginResult = await signInManager.PasswordSignInAsync(model.MachineName, model.Password, false, false);
            if (!loginResult.Succeeded)
                return new BadRequestObjectResult(new Dictionary<string, string[]> {{"errors", new[] {"Invalid credentials."}}});

            var currentUser = await userManager.FindByNameAsync(model.MachineName);
            return new ObjectResult(GenerateToken(currentUser));
        }

        private async Task<string> GenerateToken(IdentityUser user)
        {
            var role = (await userManager.GetRolesAsync(user)).First();
            var secret = securityOptions.Value.CryptographicSecret;
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret));
            var tokenExpireDays = securityOptions.Value.TokenExpireDays;
            var token = new JwtSecurityToken(
                securityOptions.Value.TokenIssuer,
                securityOptions.Value.TokenAudience,
                new[] {new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName), new Claim(ClaimTypes.Role, role)},
                DateTime.Now,
                DateTime.Now.AddDays(Convert.ToDouble(tokenExpireDays)),
                new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256)
            );
            var jwtToken = new JwtSecurityTokenHandler().WriteToken(token);

            return jwtToken;
        }

        // TODO: Format as a dictionary.
        [HttpGet("details")]
        [Authorize]
        public IActionResult TokenDetails()
        {
            var uniqueName = User.FindFirst("unique_name")?.Value;
            var role = User.FindFirst(ClaimTypes.Role)?.Value;
            return new ObjectResult(new[] {uniqueName, role});
        }
    }
}