﻿namespace CheatBuster.Server.Common.Authentication
{
    internal static class UserRoles
    {
        public static string Supervisor { get; } = "Supervisor";
        public static string MonitoredMachine { get; } = "MonitoredMachine";
    }
}