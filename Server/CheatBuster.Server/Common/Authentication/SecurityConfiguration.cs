﻿namespace CheatBuster.Server.Common.Authentication
{
    public class SecurityConfiguration
    {
        public string CryptographicSecret { get; set; }
        public string TokenIssuer { get; set; }
        public string TokenAudience { get; set; }
        public string TokenExpireDays { get; set; }
    }
}