﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CheatBuster.Server.Common.Authentication
{
    internal class UsersSeeder
    {
        public static async Task Initialize(IServiceProvider serviceProvider)
        {
            using (var authenticationContext = new AuthenticationContext(serviceProvider.GetRequiredService<DbContextOptions<AuthenticationContext>>()))
            {
                if (authenticationContext.Users.Any())
                {
                    return;
                }

                using (var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>())
                {
                    if (!await roleManager.RoleExistsAsync("Supervisor"))
                    {
                        await roleManager.CreateAsync(new IdentityRole("Supervisor"));
                    }

                    if (!await roleManager.RoleExistsAsync("MonitoredMachine"))
                    {
                        await roleManager.CreateAsync(new IdentityRole("MonitoredMachine"));
                    }
                }

                using (var userManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>())
                {
                    await userManager.CreateAsync(new IdentityUser {UserName = "admin"}, "test123");
                    await userManager.AddToRoleAsync(
                        await userManager.FindByNameAsync("admin"),
                        UserRoles.Supervisor
                    );
                    await userManager.CreateAsync(new IdentityUser {UserName = "FOO01"}, "test123");
                    await userManager.AddToRoleAsync(
                        await userManager.FindByNameAsync("FOO01"),
                        UserRoles.MonitoredMachine
                    );
                    await userManager.CreateAsync(new IdentityUser {UserName = "FOO02"}, "test123");
                    await userManager.AddToRoleAsync(
                        await userManager.FindByNameAsync("FOO02"),
                        UserRoles.MonitoredMachine
                    );
                }
            }
        }
    }
}