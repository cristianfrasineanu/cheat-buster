﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace CheatBuster.Client.Printscreen
{
    public class PrintscreenManager
    {
        private static void Main(string[] args)
        {
            var printscreen = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            var graphics = Graphics.FromImage(printscreen);
            graphics.CopyFromScreen(0, 0, 0, 0, printscreen.Size);

            if (args.Length != 2 || !System.IO.Directory.Exists(args[0]))
            {
                var appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                Directory.CreateDirectory($@"{appData}\CheatBuster");
                printscreen.Save($@"{appData}\CheatBuster\{Guid.NewGuid()}.jpeg", ImageFormat.Jpeg);
                return;
            }

            printscreen.Save($@"{args[0]}\{args[1]}.jpeg", ImageFormat.Jpeg);
        }
    }
}