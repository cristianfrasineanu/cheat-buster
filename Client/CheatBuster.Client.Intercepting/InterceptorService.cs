﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.ServiceProcess;
using System.Threading;

namespace CheatBuster.Client.Intercepting
{
    public partial class InterceptorService : ServiceBase
    {
        private readonly ClientManager clientManager;

        public new static EventLog EventLog = new EventLog();

        public InterceptorService()
        {
            try
            {
                InitializeComponent();
                CreateEventLogSource();
                clientManager = new ClientManager();
            }
            catch (Exception e)
            {
                while (e.InnerException != null) e = e.InnerException;
                EventLog.WriteEntry($"InterceptorService() -- {e.Message}", EventLogEntryType.Error);
            }
        }

        private static void CreateEventLogSource()
        {
            if (!EventLog.SourceExists("CheatBusterClient"))
                EventLog.CreateEventSource("CheatBusterClient", "CheatBuster Service");
            EventLog.Source = "CheatBusterClient";
            EventLog.Log = "CheatBuster Service";
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ro-RO");
                clientManager.InitializeMonitoringSession().Wait();
            }
            catch (Exception e)
            {
                while (e.InnerException != null) e = e.InnerException;
                EventLog.WriteEntry($"OnStart() -- {e.Message}", EventLogEntryType.Error);
            }
        }

        protected override void OnStop()
        {
            try
            {
                clientManager.FinishMonitoringSession().Wait();
            }
            catch (Exception e)
            {
                while (e.InnerException != null) e = e.InnerException;
                EventLog.WriteEntry($"OnStop() -- {e.Message}", EventLogEntryType.Error);
            }
        }
    }
}