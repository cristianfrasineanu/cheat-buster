﻿using System.ServiceProcess;

namespace CheatBuster.Client.Intercepting
{
    internal static class Bootstrap
    {
        private static void Main()
        {
            ServiceBase.Run(new InterceptorService());
        }
    }
}