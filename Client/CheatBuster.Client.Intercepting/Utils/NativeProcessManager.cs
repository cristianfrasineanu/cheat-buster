﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace CheatBuster.Client.Intercepting.Utils
{
    public static class NativeProcessManager
    {
        #region Win32 Constants

        private const int CreateUnicodeEnvironment = 0x00000400;
        private const uint InvalidSessionId = 0xFFFFFFFF;
        private static readonly IntPtr WtsCurrentServerHandle = IntPtr.Zero;
        public const uint TokenDuplicate = 0x0002;
        public const uint MaximumAllowed = 0x2000000;
        private const int StartfUseshowwindow = 0x00000001;
        private const int StartfForceonfeedback = 0x00000040;

        #endregion

        #region DllImports

        [DllImport("advapi32.dll", EntryPoint = "CreateProcessAsUser", SetLastError = true, CharSet = CharSet.Ansi,
            CallingConvention = CallingConvention.StdCall)]
        private static extern bool CreateProcessAsUser(
            IntPtr hToken,
            string lpApplicationName,
            string lpCommandLine,
            IntPtr lpProcessAttributes,
            IntPtr lpThreadAttributes,
            bool bInheritHandle,
            uint dwCreationFlags,
            IntPtr lpEnvironment,
            string lpCurrentDirectory,
            ref Startupinfo lpStartupInfo,
            out ProcessInformation lpProcessInformation);

        [DllImport("advapi32.dll", EntryPoint = "DuplicateTokenEx")]
        private static extern bool DuplicateTokenEx(
            IntPtr existingTokenHandle,
            uint dwDesiredAccess,
            IntPtr lpThreadAttributes,
            int tokenType,
            int impersonationLevel,
            ref IntPtr duplicateTokenHandle);

        [DllImport("userenv.dll", SetLastError = true)]
        private static extern bool CreateEnvironmentBlock(ref IntPtr lpEnvironment, IntPtr hToken, bool bInherit);

        [DllImport("userenv.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool DestroyEnvironmentBlock(IntPtr lpEnvironment);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool CloseHandle(IntPtr hSnapshot);

        [DllImport("kernel32.dll")]
        private static extern uint WTSGetActiveConsoleSessionId();

        [DllImport("Wtsapi32.dll")]
        private static extern uint WTSQueryUserToken(uint sessionId, ref IntPtr phToken);

        [DllImport("wtsapi32.dll", SetLastError = true)]
        private static extern int WTSEnumerateSessions(
            IntPtr hServer,
            int reserved,
            int version,
            ref IntPtr ppSessionInfo,
            ref int pCount);

        #endregion

        #region Win32 Structs

        private enum Sw
        {
            SwHide = 0,
            SwShownormal = 1,
            SwNormal = 1,
            SwShowminimized = 2,
            SwShowmaximized = 3,
            SwMaximize = 3,
            SwShownoactivate = 4,
            SwShow = 5,
            SwMinimize = 6,
            SwShowminnoactive = 7,
            SwShowna = 8,
            SwRestore = 9,
            SwShowdefault = 10,
            SwMax = 10
        }

        private enum WtsConnectstateClass
        {
            WtsActive,
            WtsConnected,
            WtsConnectQuery,
            WtsShadow,
            WtsDisconnected,
            WtsIdle,
            WtsListen,
            WtsReset,
            WtsDown,
            WtsInit
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct ProcessInformation
        {
            public readonly IntPtr hProcess;
            public readonly IntPtr hThread;
            public readonly uint dwProcessId;
            public readonly uint dwThreadId;
        }

        private enum SecurityImpersonationLevel
        {
            SecurityAnonymous = 0,
            SecurityIdentification = 1,
            SecurityImpersonation = 2,
            SecurityDelegation = 3
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct Startupinfo
        {
            public int cb;
            public readonly string lpReserved;
            public string lpDesktop;
            public readonly string lpTitle;
            public readonly uint dwX;
            public readonly uint dwY;
            public readonly uint dwXSize;
            public readonly uint dwYSize;
            public readonly uint dwXCountChars;
            public readonly uint dwYCountChars;
            public readonly uint dwFillAttribute;
            public uint dwFlags;
            public short wShowWindow;
            public readonly short cbReserved2;
            public readonly IntPtr lpReserved2;
            public readonly IntPtr hStdInput;
            public readonly IntPtr hStdOutput;
            public readonly IntPtr hStdError;
        }

        private enum TokenType
        {
            TokenPrimary = 1,
            TokenImpersonation = 2
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct WtsSessionInfo
        {
            public readonly uint SessionID;

            [MarshalAs(UnmanagedType.LPStr)] public readonly string pWinStationName;

            public readonly WtsConnectstateClass State;
        }

        [Flags]
        public enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VirtualMemoryOperation = 0x00000008,
            VirtualMemoryRead = 0x00000010,
            DuplicateHandle = 0x00000040,
            CreateProcess = 0x000000080,
            SetQuota = 0x00000100,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            QueryLimitedInformation = 0x00001000,
            Synchronize = 0x00100000
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct SecurityAttributes
        {
            public int nLength;
            public IntPtr lpSecurityDescriptor;
            public int bInheritHandle;
        }

        #endregion

        private static bool GetSessionUserToken(ref IntPtr phUserToken)
        {
            var bResult = false;
            var hImpersonationToken = IntPtr.Zero;
            var activeSessionId = InvalidSessionId;
            var pSessionInfo = IntPtr.Zero;
            var sessionCount = 0;

            if (WTSEnumerateSessions(WtsCurrentServerHandle, 0, 1, ref pSessionInfo, ref sessionCount) != 0)
            {
                var arrayElementSize = Marshal.SizeOf(typeof(WtsSessionInfo));
                var current = pSessionInfo;

                for (var i = 0; i < sessionCount; i++)
                {
                    var sessionInfo = (WtsSessionInfo) Marshal.PtrToStructure(current, typeof(WtsSessionInfo));
                    current += arrayElementSize;

                    if (sessionInfo.State == WtsConnectstateClass.WtsActive) activeSessionId = sessionInfo.SessionID;
                }
            }

            if (activeSessionId == InvalidSessionId) activeSessionId = WTSGetActiveConsoleSessionId();

            if (WTSQueryUserToken(activeSessionId, ref hImpersonationToken) != 0)
            {
                bResult = DuplicateTokenEx(hImpersonationToken, 0, IntPtr.Zero,
                    (int) SecurityImpersonationLevel.SecurityImpersonation, (int) TokenType.TokenPrimary,
                    ref phUserToken);
                CloseHandle(hImpersonationToken);
            }

            return bResult;
        }

        public static bool StartProcessAsCurrentUser(string appPath, string cmdLine = null, string workDir = null)
        {
            var hUserToken = IntPtr.Zero;
            var startInfo = new Startupinfo();
            var procInfo = new ProcessInformation();
            var pEnv = IntPtr.Zero;

            startInfo.cb = Marshal.SizeOf(typeof(Startupinfo));
            startInfo.wShowWindow = (short) Sw.SwShow;
            startInfo.lpDesktop = "winsta0\\default";
            startInfo.dwFlags = StartfUseshowwindow | StartfForceonfeedback;

            try
            {
                if (!GetSessionUserToken(ref hUserToken))
                    throw new Exception("StartProcessAsCurrentUser: GetSessionUserToken failed.");

                if (!CreateEnvironmentBlock(ref pEnv, hUserToken, false))
                    throw new Exception("StartProcessAsCurrentUser: CreateEnvironmentBlock failed.");

                if (!CreateProcessAsUser(hUserToken, appPath, cmdLine, IntPtr.Zero, IntPtr.Zero, false,
                    CreateUnicodeEnvironment, pEnv, workDir, ref startInfo, out procInfo))
                    throw new Exception("StartProcessAsCurrentUser: CreateProcessAsUser failed: " +
                                        new Win32Exception(Marshal.GetLastWin32Error()).Message);
            }
            finally
            {
                CloseHandle(hUserToken);
                if (pEnv != IntPtr.Zero) DestroyEnvironmentBlock(pEnv);

                CloseHandle(procInfo.hThread);
                CloseHandle(procInfo.hProcess);
            }

            return true;
        }
    }
}