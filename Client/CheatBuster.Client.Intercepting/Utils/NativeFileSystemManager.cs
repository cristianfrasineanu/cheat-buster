﻿using System.Runtime.InteropServices;

namespace CheatBuster.Client.Intercepting.Utils
{
    public static class NativeFileSystemManager
    {
        #region DllImports

        [DllImport("kernel32.dll")]
        public static extern bool SetCurrentDirectory(string lpPathName);

        #endregion
    }
}