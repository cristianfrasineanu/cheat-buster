﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.Windows;
using System.Xml;

namespace CheatBuster.Client.Intercepting
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        private string _serverAddress;

        private string ServerAddress
        {
            get => _serverAddress;
            set
            {
                if (value.Length == 0) throw new Exception("The server address cannot be null.");
                _serverAddress = value;
            }
        }

        private string _statusEndpoint;

        private string StatusEndpoint
        {
            get => _statusEndpoint;
            set
            {
                if (value.Length == 0) throw new Exception("The push endpoint cannot be null.");
                _statusEndpoint = value;
            }
        }

        private string _machineName;
        private string _password;

        public ProjectInstaller()
        {
            InitializeComponent();
        }

        private void serviceInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
        }

        private void serviceProcessInstaller1_AfterInstall(object sender, InstallEventArgs e)
        {
        }

        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);

            try
            {
                AddConfigurationDetails();
                ProtectCredentials();
            }
            catch (Exception e)
            {
                MessageBox.Show($"Failed to create the service configuration file:  {e.Message}");
                Rollback(savedState);
            }
        }

        private void ProtectCredentials()
        {
            var configuration = ConfigurationManager.OpenExeConfiguration(Context.Parameters["assemblypath"]);
            var appSettings = configuration.GetSection("appSettings");
            if (!appSettings.SectionInformation.IsProtected)
            {
                appSettings.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
                configuration.Save(ConfigurationSaveMode.Modified, true);
            }
        }

        private void AddConfigurationDetails()
        {
            TransferParametersValues();

            var assemblypath = Context.Parameters["assemblypath"];
            var appConfigPath = assemblypath + ".config";

            var xml = new XmlDocument();
            xml.Load(appConfigPath);

            XmlNode configuration = null;
            foreach (XmlNode node in xml.ChildNodes)
                if (node.Name == "configuration")
                    configuration = node;
            if (configuration != null)
            {
                GenerateConnectionStrings(xml, configuration);
                GenerateAppSettings(xml, configuration);
                xml.Save(appConfigPath);
                return;
            }

            throw new Exception("Could not find the configuration node.");
        }

        private void TransferParametersValues()
        {
            ServerAddress = Context.Parameters["ServerAddress"];
            StatusEndpoint = Context.Parameters["StatusEndpoint"];
            _machineName = Context.Parameters["MachineName"];
            _password = Context.Parameters["Password"];
        }

        private void GenerateConnectionStrings(XmlDocument xml, XmlNode configuration)
        {
            XmlNode connectionStringsNode = xml.CreateNode(XmlNodeType.Element, "connectionStrings", "");
            XmlAttribute nameAttribute = xml.CreateAttribute("name");
            XmlAttribute connectionStringAttribute = xml.CreateAttribute("connectionString");

            var baseServerAddressNode = xml.CreateNode(XmlNodeType.Element, "add", "");
            nameAttribute.Value = "baseServerAddress";
            connectionStringAttribute.Value = ServerAddress;
            baseServerAddressNode.Attributes?.Append(nameAttribute);
            baseServerAddressNode.Attributes?.Append(connectionStringAttribute);
            connectionStringsNode.AppendChild(baseServerAddressNode);

            var statusEndpointNode = xml.CreateNode(XmlNodeType.Element, "add", "");
            nameAttribute = xml.CreateAttribute("name");
            nameAttribute.Value = "statusEndpoint";
            connectionStringAttribute = xml.CreateAttribute("connectionString");
            connectionStringAttribute.Value = StatusEndpoint;
            statusEndpointNode.Attributes?.Append(nameAttribute);
            statusEndpointNode.Attributes?.Append(connectionStringAttribute);
            connectionStringsNode.AppendChild(statusEndpointNode);

            configuration.AppendChild(connectionStringsNode);
        }

        private void GenerateAppSettings(XmlDocument xml, XmlNode configuration)
        {
            XmlNode settingNode = xml.CreateNode(XmlNodeType.Element, "appSettings", "");
            XmlAttribute keyAttribute = xml.CreateAttribute("key");
            XmlAttribute valueAttribute = xml.CreateAttribute("value");

            var machineNameElement = xml.CreateNode(XmlNodeType.Element, "add", "");
            keyAttribute.Value = "machineName";
            valueAttribute.Value = _machineName;
            if (_machineName.Length == 0) valueAttribute.Value = Environment.MachineName;
            machineNameElement.Attributes?.Append(keyAttribute);
            machineNameElement.Attributes?.Append(valueAttribute);
            settingNode.AppendChild(machineNameElement);

            var passwordElement = xml.CreateNode(XmlNodeType.Element, "add", "");
            keyAttribute = xml.CreateAttribute("key");
            keyAttribute.Value = "password";
            valueAttribute = xml.CreateAttribute("value");
            valueAttribute.Value = _password;
            passwordElement.Attributes?.Append(keyAttribute);
            passwordElement.Attributes?.Append(valueAttribute);
            settingNode.AppendChild(passwordElement);

            configuration.AppendChild(settingNode);
        }
    }
}