﻿namespace CheatBuster.Client.Intercepting
{
    internal interface IClientConfiguration
    {
        string LoadMachineName();

        string LoadPassword();

        string LoadServerAddress();

        string LoadWebSocketServicePath();

        void SavePassword(string password);
    }
}