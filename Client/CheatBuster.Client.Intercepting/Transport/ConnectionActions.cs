﻿namespace CheatBuster.Client.Intercepting.Transport
{
    internal enum ConnectionActions
    {
        Authenticate,
        Register,
        LoadUserDetails
    }
}
