﻿using System.Threading.Tasks;

namespace CheatBuster.Client.Intercepting.Transport.Registration
{
    internal interface IRegistration
    {
        Task<bool> Register(object options);
    }
}
