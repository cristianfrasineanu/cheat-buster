﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using CheatBuster.Common.Models;

namespace CheatBuster.Client.Intercepting.Transport.Registration
{
    internal class UsernamePasswordRegistration : IRegistration
    {
        private readonly HttpClient client;

        public UsernamePasswordRegistration(HttpClient client)
        {
            this.client = client;
        }

        public async Task<bool> Register(object options)
        {
            if (!(options is MachineAuthentication)) throw new Exception("Unsupported options format.");

            HttpResponseMessage response;
            var authenticationDetails = (MachineAuthentication) options;

            try
            {
                response = await client.PostAsJsonAsync(ConnectionRoutes.Actions[ConnectionActions.Register], authenticationDetails);
            }
            catch (Exception e)
            {
                InterceptorService.EventLog.WriteEntry($"Register() -- {e.Message}", EventLogEntryType.Error);
                return false;
            }

            return response.IsSuccessStatusCode;
        }
    }
}