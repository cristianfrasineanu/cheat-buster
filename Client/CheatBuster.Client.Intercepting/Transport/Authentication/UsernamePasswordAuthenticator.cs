﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CheatBuster.Common.Models;
using Newtonsoft.Json;

namespace CheatBuster.Client.Intercepting.Transport.Authentication
{
    internal class UsernamePasswordAuthenticator : IAuthenticator
    {
        private readonly HttpClient client;

        public UsernamePasswordAuthenticator(HttpClient client)
        {
            this.client = client;
        }

        public async Task<bool> SignIn(object credentials)
        {
            if (!(credentials is MachineAuthentication)) throw new Exception("Unsupported credentials format.");

            HttpResponseMessage response;
            var authenticationDetails = (MachineAuthentication) credentials;

            try
            {
                response = await client.PostAsJsonAsync(ConnectionRoutes.Actions[ConnectionActions.Authenticate], authenticationDetails);
            }
            catch (Exception e)
            {
                InterceptorService.EventLog.WriteEntry($"SignIn() -- {e.Message}", EventLogEntryType.Error);
                return false;
            }

            if (response.IsSuccessStatusCode) return await ConfigureAuthorizationHeader(response);

            return false;
        }

        public Task<bool> SignOut()
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "");
            return Task.FromResult(true);
        }

        private async Task<bool> ConfigureAuthorizationHeader(HttpResponseMessage response)
        {
            var serverResponse = await response.Content.ReadAsStringAsync();
            var parsedResponse = JsonConvert.DeserializeObject<Dictionary<string, string>>(serverResponse);
            if (bool.Parse(parsedResponse["isCompletedSuccessfully"]) && !bool.Parse(parsedResponse["isFaulted"]))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", parsedResponse["result"]);
                return true;
            }

            InterceptorService.EventLog.WriteEntry("ConfigureAuthorizationHeader() -- Invalid token received.", EventLogEntryType.Error);
            return false;
        }
    }
}