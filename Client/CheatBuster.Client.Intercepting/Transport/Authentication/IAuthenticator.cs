﻿using System.Threading.Tasks;

namespace CheatBuster.Client.Intercepting.Transport.Authentication
{
    internal interface IAuthenticator
    {
        Task<bool> SignIn(object credentials);
        Task<bool> SignOut();
    }
}