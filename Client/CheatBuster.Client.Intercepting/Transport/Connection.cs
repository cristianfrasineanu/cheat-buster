﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Security;
using CheatBuster.Client.Intercepting.Transport.Authentication;
using CheatBuster.Client.Intercepting.Transport.Registration;
using CheatBuster.Common.Models;
using CheatBuster.Common.Models.WebSocket.Messages;
using Newtonsoft.Json;

namespace CheatBuster.Client.Intercepting.Transport
{
    internal class Connection
    {
        private static readonly int ConnectionRetryInterval = (int) TimeSpan.FromMinutes(1).TotalMilliseconds;

        private readonly ClientWebSocket websocketClient;
        private readonly HttpClient httpClient;
        private readonly IClientConfiguration clientConfiguration = new ClientConfiguration();

        private readonly IAuthenticator authenticator;
        private readonly IRegistration registration;
        private string AuthToken { get; set; }

        #region Events

        public event Action Authenticated;
        public event Action Disconnected;
        public event Action<Message<string>> MessageReceived;

        protected virtual void OnAuthenticated()
        {
            var task = LoadUserDetails();
            task.Wait();
            var userDetails = task.Result;

            InterceptorService.EventLog.WriteEntry($"Successfully authenticated {string.Join(" ", userDetails)}", EventLogEntryType.Information);
            Authenticated?.Invoke();
        }

        protected virtual void OnDisconnected()
        {
            Disconnected?.Invoke();
        }

        protected virtual void OnMessageReceived(Message<string> obj)
        {
            MessageReceived?.Invoke(obj);
        }

        #endregion

        public Connection()
        {
            var baseServerUrl = clientConfiguration.LoadServerAddress();
            httpClient = new HttpClient {BaseAddress = new Uri($"http://{baseServerUrl}")};
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.Timeout = TimeSpan.FromSeconds(10);
            websocketClient = new ClientWebSocket();

            authenticator = new UsernamePasswordAuthenticator(httpClient);
            registration = new UsernamePasswordRegistration(httpClient);
        }

        public async Task InitiateConnection()
        {
            var username = clientConfiguration.LoadMachineName();
            var password = clientConfiguration.LoadPassword();

            if (password.Length == 0)
            {
                if (!await RegisterNewMachine(username))
                {
                    InterceptorService.EventLog.WriteEntry($"InitiateConnection() -- Failed to register new machine as {username}. Retrying in {ConnectionRetryInterval / 1000} seconds...", EventLogEntryType.Error);
                    Thread.Sleep(ConnectionRetryInterval);
#pragma warning disable 4014
                    InitiateConnection();
#pragma warning restore 4014
                    return;
                }

                password = clientConfiguration.LoadPassword();
            }

            if (!await AuthenticateMachine(username, password))
            {
                InterceptorService.EventLog.WriteEntry($"InitiateConnection() -- Failed to authenticate machine as {username}. Retrying in {ConnectionRetryInterval / 1000} seconds...", EventLogEntryType.Error);
                Thread.Sleep(ConnectionRetryInterval);
#pragma warning disable 4014
                InitiateConnection();
#pragma warning restore 4014
                return;
            }

            await ConnectToWebSocketService();

            OnAuthenticated();
        }

        public async Task CloseConnection()
        {
            await authenticator.SignOut();
            if (websocketClient.State == WebSocketState.Open)
            {
                try
                {
                    InterceptorService.EventLog.WriteEntry("Performing close handshake with the WebSocket service...", EventLogEntryType.Information);
                    await websocketClient.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closed by the client.", CancellationToken.None);
                    InterceptorService.EventLog.WriteEntry("Client disconnected.", EventLogEntryType.Information);
                }
                catch (Exception e)
                {
                    InterceptorService.EventLog.WriteEntry($"CloseConnection() -- Failed to perform close handshake with the server: {e.Message}", EventLogEntryType.Error);
                }
            }

            OnDisconnected();
        }

        public async Task<HttpResponseMessage> SendPost(string endpoint, object payload) => await httpClient.PostAsJsonAsync(endpoint, payload);

        public async Task<HttpResponseMessage> SendPost(string endpoint, object payload, MediaTypeFormatter formatter) => await httpClient.PostAsync(endpoint, payload, formatter);

        public async Task<HttpResponseMessage> SendGet(string endpoint) => await httpClient.GetAsync(endpoint);

        public async Task SendMessage(string message)
        {
            if (websocketClient.State != WebSocketState.Open)
            {
                InterceptorService.EventLog.WriteEntry("SendMessage() -- Unable to send WebSocket message.", EventLogEntryType.Error);
                return;
            }

            await websocketClient.SendAsync(
                new ArraySegment<byte>(Encoding.ASCII.GetBytes(message), 0, message.Length),
                WebSocketMessageType.Text,
                true,
                CancellationToken.None
            );
        }

        private async Task<string[]> LoadUserDetails()
        {
            var response = await httpClient.GetAsync(ConnectionRoutes.Actions[ConnectionActions.LoadUserDetails]);
            if (!response.IsSuccessStatusCode)
            {
                InterceptorService.EventLog.WriteEntry($"LoadUserDetails() -- The request failed with status {response.StatusCode}.", EventLogEntryType.Error);
                return null;
            }

            var serverResponse = await response.Content.ReadAsStringAsync();
            var details = JsonConvert.DeserializeObject<string[]>(serverResponse);
            return details;
        }

        private async Task ConnectToWebSocketService()
        {
            if (AuthToken == null)
            {
                InterceptorService.EventLog.WriteEntry("ConnectToWebSocketService() -- Unexpected null auth token.", EventLogEntryType.Error);
                return;
            }

            await websocketClient.ConnectAsync(
                new Uri("ws://" +
                        $"{clientConfiguration.LoadServerAddress()}" +
                        $"{clientConfiguration.LoadWebSocketServicePath()}" +
                        $"?accessToken={AuthToken}"),
                CancellationToken.None
            );

#pragma warning disable 4014
            ReceiveMessage();
#pragma warning restore 4014
        }

        private async Task ReceiveMessage()
        {
            var buffer = new byte[1024 * 4];

            while (websocketClient.State == WebSocketState.Open)
            {
                try
                {
                    var result = await websocketClient.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);

                    switch (result.MessageType)
                    {
                        case WebSocketMessageType.Close:
                            InterceptorService.EventLog.WriteEntry("Received close message.", EventLogEntryType.Information);
                            await websocketClient.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closed by the client.", CancellationToken.None);
                            break;
                        case WebSocketMessageType.Text:
                            HandleMessage(result, buffer);
                            break;
                        case WebSocketMessageType.Binary:
                            InterceptorService.EventLog.WriteEntry("Unsupported WebSocket message type.", EventLogEntryType.Error);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
                catch (Exception e)
                {
                    InterceptorService.EventLog.WriteEntry("ReceiveMessage() -- " + e.Message, EventLogEntryType.Error);
                    await websocketClient.CloseAsync(WebSocketCloseStatus.InternalServerError, "Unexpected error when handling the server message", CancellationToken.None);
                    break;
                }
            }
        }

        private void HandleMessage(WebSocketReceiveResult result, byte[] buffer)
        {
            var payload = Encoding.UTF8.GetString(buffer, 0, result.Count);
            Message<string> message;
            try
            {
                message = MessageFormatter.DeserializeMessage<string>(payload);
            }
            catch (Exception)
            {
                InterceptorService.EventLog.WriteEntry("HandleMessage() -- unsupported server message format.", EventLogEntryType.Error);
                return;
            }

            OnMessageReceived(message);
        }

        private async Task<bool> RegisterNewMachine(string username)
        {
            var password = Membership.GeneratePassword(8, 1);
            var options = new MachineAuthentication
            {
                MachineName = username,
                Password = password
            };
            if (!await registration.Register(options)) return false;

            clientConfiguration.SavePassword(password);
            return true;
        }

        private async Task<bool> AuthenticateMachine(string username, string password)
        {
            var credentials = new MachineAuthentication
            {
                MachineName = username,
                Password = password
            };
            if (!await authenticator.SignIn(credentials)) return false;

            AuthToken = httpClient.DefaultRequestHeaders.Authorization.Parameter;
            return true;
        }
    }
}