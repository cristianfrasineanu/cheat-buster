﻿using System.Collections.Generic;

namespace CheatBuster.Client.Intercepting.Transport
{
    internal class ConnectionRoutes
    {
        public static Dictionary<ConnectionActions, string> Actions = new Dictionary<ConnectionActions, string>
        {
            {ConnectionActions.Authenticate, "/authenticate"},
            {ConnectionActions.Register, "/client/register"},
            {ConnectionActions.LoadUserDetails, "/details"}
        };
    }
}