﻿using System.Configuration;
using System.Reflection;
using static System.IO.Path;

namespace CheatBuster.Client.Intercepting
{
    internal class ClientConfiguration : IClientConfiguration
    {
        private static readonly string ServiceAssemblyLocation = Assembly.GetEntryAssembly().Location;

        private readonly Configuration currentConfiguration;

        public ClientConfiguration()
        {
            currentConfiguration = ConfigurationManager.OpenExeConfiguration(GetFullPath(ServiceAssemblyLocation));
        }

        public string LoadMachineName()
        {
            return currentConfiguration.AppSettings.Settings["machineName"].Value;
        }

        public string LoadPassword()
        {
            return currentConfiguration.AppSettings.Settings["password"].Value;
        }

        public void SavePassword(string password)
        {
            currentConfiguration.AppSettings.Settings["password"].Value = password;
            currentConfiguration.Save(ConfigurationSaveMode.Modified, true);
        }

        public string LoadServerAddress()
        {
            return currentConfiguration.ConnectionStrings.ConnectionStrings["baseServerAddress"].ConnectionString;
        }

        public string LoadWebSocketServicePath()
        {
            return currentConfiguration.ConnectionStrings.ConnectionStrings["statusEndpoint"].ConnectionString;
        }
    }
}