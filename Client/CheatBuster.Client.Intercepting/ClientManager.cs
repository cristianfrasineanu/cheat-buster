﻿using System.Threading.Tasks;
using CheatBuster.Client.Intercepting.Session;
using CheatBuster.Client.Intercepting.Transport;
using CheatBuster.Common.Models.WebSocket.Messages;

namespace CheatBuster.Client.Intercepting
{
    internal class ClientManager
    {
        private readonly Connection connection;
        private readonly SessionController sessionController;

        public ClientManager()
        {
            connection = new Connection();
            sessionController = new SessionController(connection);

            connection.Authenticated += OnAuthenticated;
            connection.Disconnected += OnDisconnected;
            connection.MessageReceived += OnMessageReceived;
        }

        public async Task InitializeMonitoringSession()
        {
            sessionController.InitializeSession();
            await connection.InitiateConnection();
        }

        public async Task FinishMonitoringSession()
        {
            sessionController.DestroySession();
            await connection.CloseConnection();
        }

        private void OnAuthenticated()
        {
            sessionController.HandleAuthenticated().Wait();
        }

        private void OnDisconnected()
        {
            sessionController.HandleDisconnected();
        }

        private void OnMessageReceived(Message<string> serverMessage)
        {
            sessionController.HandleMessageReceived(serverMessage);
        }
    }
}