﻿using CheatBuster.Common.Models.Monitoring;

namespace CheatBuster.Client.Intercepting.Monitoring.Model
{
    public class Rule : RuleBase
    {
        public override string ToString()
        {
            return $"{Filter} - {Action}";
        }
    }
}