﻿namespace CheatBuster.Client.Intercepting.Monitoring.Monitors
{
    internal interface IMonitor
    {
        void Start();
        void Stop();
    }

    internal delegate void MonitorEventHandler(MonitorEvent monitorEvent);
}