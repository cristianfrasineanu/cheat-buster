﻿using System;
using System.Collections.Generic;
using CheatBuster.Client.Intercepting.Monitoring.Monitors.System.Event;
using CheatBuster.Client.Intercepting.Monitoring.Monitors.System.Process;
using CheatBuster.Common.Models.Monitoring;

namespace CheatBuster.Client.Intercepting.Monitoring.Monitors
{
    internal static class RecognizedFilters
    {
        private static readonly Dictionary<string, Type> SystemEventFilters = new Dictionary<string, Type>
        {
            {"USB_Storage_Device", typeof(UsbStorageDeviceMonitor)},
            {"Desktop_Activity", typeof(DesktopActivityMonitor)}
        };

        private static readonly Dictionary<string, Type> SystemProcessFilters = new Dictionary<string, Type>
        {
            {"Internet_Browsing", typeof(InternetBrowsingMonitor)},
            {"Document_Management", typeof(DocumentManagementMonitor)}
        };

        // TODO
        private static readonly Dictionary<string, Type> NetworkFilters = new Dictionary<string, Type>();

        public static Dictionary<ActionType, Dictionary<string, Type>> Actions { get; } = new Dictionary<ActionType, Dictionary<string, Type>>
        {
            {ActionType.SystemEvent, SystemEventFilters},
            {ActionType.SystemProcess, SystemProcessFilters},
            {ActionType.WebRequest, NetworkFilters},
            {ActionType.ProtocolUsage, null}
        };
    }
}