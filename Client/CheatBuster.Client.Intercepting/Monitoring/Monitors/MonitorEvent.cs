﻿using System;

namespace CheatBuster.Client.Intercepting.Monitoring.Monitors
{
    internal class MonitorEvent
    {
        public Type Type { get; set; }
        public string Details { get; set; }
    }
}