﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;

namespace CheatBuster.Client.Intercepting.Monitoring.Monitors.System.Process
{
    internal class DocumentManagementMonitor : IMonitor
    {
        private static readonly string ProcessStartTraceQuery = "SELECT * FROM Win32_ProcessStartTrace";
        private static readonly IList<string> RestrictedProcesses = new List<string> {"acrord", "winword", "notepad", "wordpad", "excel"};

        private readonly ManagementEventWatcher watcher;
        private bool isAttached;

        private readonly MonitorEventHandler handle;

        public DocumentManagementMonitor(MonitorEventHandler handle)
        {
            this.handle = handle;

            watcher = new ManagementEventWatcher {Query = new WqlEventQuery(ProcessStartTraceQuery)};
            watcher.EventArrived += HandleProcessStarted;
        }

        private void HandleProcessStarted(object sender, EventArrivedEventArgs e)
        {
            var processName = e.NewEvent.Properties["ProcessName"].Value.ToString();
            var isRestricted = RestrictedProcesses.First(r => processName.ToLower().Contains(r));
            if (isRestricted != null)
                handle(new MonitorEvent {Type = typeof(InternetBrowsingMonitor), Details = $"A restricted process ({processName}) was started."});
        }

        public void Start()
        {
            if (isAttached) return;

            watcher.Start();
            isAttached = true;
            InterceptorService.EventLog.WriteEntry("Monitoring for document management processes.", EventLogEntryType.Information);
        }

        public void Stop()
        {
            if (!isAttached) return;

            watcher.Stop();
            isAttached = false;
            InterceptorService.EventLog.WriteEntry("Stopped monitoring for document management processes.", EventLogEntryType.Information);
        }
    }
}