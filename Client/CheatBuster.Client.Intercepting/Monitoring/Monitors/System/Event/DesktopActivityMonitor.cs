﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Timers;
using CheatBuster.Client.Intercepting.Utils;

namespace CheatBuster.Client.Intercepting.Monitoring.Monitors.System.Event
{
    internal class DesktopActivityMonitor : IMonitor
    {
        private static readonly string CurrentAssemblyDirectory = Path.GetDirectoryName(Path.GetFullPath(typeof(DesktopActivityMonitor).Assembly.Location));
        private static readonly string PrintScreenAssemblyLocation = typeof(Printscreen.PrintscreenManager).Assembly.Location;
        private static readonly double ScreenshotInterval = TimeSpan.FromSeconds(30).TotalMilliseconds;
        private const string ScreenshotsFolderName = "screenshots";

        private readonly Timer screnshotTimer;
        private readonly string screenshotsDirectoryPath = $@"{CurrentAssemblyDirectory}\{ScreenshotsFolderName}";
        private readonly string screenshotExecutablePath = Path.GetFullPath(PrintScreenAssemblyLocation);

        private FileSystemWatcher fileWatcher;

        private readonly IClientConfiguration clientConfiguration = new ClientConfiguration();

        public DesktopActivityMonitor(MonitorEventHandler handle)
        {
            Directory.CreateDirectory(screenshotsDirectoryPath);

            screnshotTimer = new Timer
            {
                Interval = ScreenshotInterval,
                AutoReset = true
            };
            screnshotTimer.Elapsed += (sender, args) => { HandleScreenshot(handle); };
        }

        // TODO: Add Readme notice about folder permissions.
        private void HandleScreenshot(MonitorEventHandler handle)
        {
            var filename = clientConfiguration.LoadMachineName() + DateTime.Now.ToString("yyyyMMddHHmmssffff");

            try
            {
                var escapedScreenshotDirectoryPath = "\"" + Regex.Replace(screenshotsDirectoryPath, @"(\\+)$", @"$1$1") + "\"";
                var result = NativeProcessManager.StartProcessAsCurrentUser(screenshotExecutablePath, $@"{typeof(Printscreen.PrintscreenManager).Assembly.GetName().Name} {escapedScreenshotDirectoryPath} {filename}");
                if (!result)
                {
                    InterceptorService.EventLog.WriteEntry("HandleScreenshot() -- Failed to launch the screenshot child process.", EventLogEntryType.Error);
                    return;
                }

                if (fileWatcher != null) fileWatcher.EnableRaisingEvents = false;
                fileWatcher = new FileSystemWatcher
                {
                    Path = screenshotsDirectoryPath,
                    NotifyFilter = NotifyFilters.LastWrite,
                    Filter = $"{filename}.jpeg"
                };
                fileWatcher.Changed += (sender, args) =>
                {
                    // Prevent multiple Changed events.
                    fileWatcher.EnableRaisingEvents = false;

                    var screenshotFile = new FileInfo($@"{screenshotsDirectoryPath}\{filename}.jpeg");
                    while (IsFileLocked(screenshotFile))
                    {
                    }

                    handle(new MonitorEvent {Type = typeof(DesktopActivityMonitor), Details = $@"{screenshotsDirectoryPath}\{filename}.jpeg"});
                };
                fileWatcher.EnableRaisingEvents = true;
            }
            catch (Exception e)
            {
                InterceptorService.EventLog.WriteEntry($"HandleScreenshot() -- {e.Message}", EventLogEntryType.Error);
            }
        }

        private bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                stream?.Close();
            }

            return false;
        }

        public void Start()
        {
            if (screnshotTimer.Enabled) return;
            screnshotTimer.Start();

            InterceptorService.EventLog.WriteEntry($@"Taking screenshots every {ScreenshotInterval / 1000} seconds in {screenshotsDirectoryPath}", EventLogEntryType.Information);
        }

        public void Stop()
        {
            if (!screnshotTimer.Enabled) return;
            screnshotTimer.Stop();

            InterceptorService.EventLog.WriteEntry("Stopped taking screenshots.", EventLogEntryType.Information);
        }
    }
}