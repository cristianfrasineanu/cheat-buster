﻿using System.Diagnostics;
using System.Management;

namespace CheatBuster.Client.Intercepting.Monitoring.Monitors.System.Event
{
    internal class UsbStorageDeviceMonitor : IMonitor
    {
        private static readonly string UsbStorageDeviceQuery = "SELECT * FROM Win32_VolumeChangeEvent WHERE EventType = 2";

        private readonly ManagementEventWatcher watcher;
        private bool isAttached;

        public UsbStorageDeviceMonitor(MonitorEventHandler handle)
        {
            watcher = new ManagementEventWatcher {Query = new WqlEventQuery(UsbStorageDeviceQuery)};
            watcher.EventArrived += (sender, e) => handle(new MonitorEvent { Type = typeof(UsbStorageDeviceMonitor), Details = "A USB storage device was plugged in."});
        }

        public void Start()
        {
            if (isAttached) return;

            watcher.Start();
            isAttached = true;
            InterceptorService.EventLog.WriteEntry("Monitoring for USB storage devices.", EventLogEntryType.Information);
        }

        public void Stop()
        {
            if (!isAttached) return;

            watcher.Stop();
            isAttached = false;
            InterceptorService.EventLog.WriteEntry("Stopped monitoring for USB storage devices.", EventLogEntryType.Information);
        }
    }
}