﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using PcapDotNet.Core;
using PcapDotNet.Packets;
using PcapDotNet.Packets.IpV4;
using Exception = System.Exception;

namespace CheatBuster.Client.Intercepting.Monitoring.Monitors.Network
{
    internal class WebRequestMonitor : IMonitor
    {
        private readonly Regex filterPattern;

        private readonly IDictionary<Task, PacketCommunicator> communicators = new Dictionary<Task, PacketCommunicator>();
        private bool isSniffing;

        private readonly MonitorEventHandler handle;

        public WebRequestMonitor(MonitorEventHandler handle, string filterPattern)
        {
            try
            {
                this.filterPattern = new Regex(filterPattern, RegexOptions.IgnoreCase);
            }
            catch (Exception)
            {
                InterceptorService.EventLog.WriteEntry("WebRequestMonitor() -- Failed to register the hostname filter pattern.", EventLogEntryType.Error);
                return;
            }

            this.handle = handle;

            var packetDevices = LivePacketDevice.AllLocalMachine;
            foreach (var livePacketDevice in packetDevices)
            {
                if ((livePacketDevice.Attributes & DeviceAttributes.Loopback) != 0) continue;

                var communicator = livePacketDevice.Open();
                communicator.SetFilter(communicator.CreateFilter("tcp"));
                communicators.Add(new Task(() => communicator.ReceivePackets(0, HandlePacketData)), communicator);
            }
        }

        // TODO: Check if the Wi-Fi packets are handled correctly.
        private void HandlePacketData(Packet packet)
        {
            var ethernetIpV4 = packet.Ethernet.IpV4;

            try
            {
                if (ethernetIpV4.Protocol == IpV4Protocol.Tcp)
                {
                    var tcpDatagram = ethernetIpV4.Tcp;

                    if (tcpDatagram.DestinationPort == 80 && tcpDatagram.Http.IsRequest && tcpDatagram.Http.Header != null)
                    {
                        var filterMatch = filterPattern.Match(tcpDatagram.Http.Header.ToString());
                        if (filterMatch.Success)
                        {
                            var groupValue = filterMatch.Groups[0].Value;
                            handle(new MonitorEvent {Type = typeof(WebRequestMonitor), Details = $"Restricted hostname identified while making an HTTP request to: {groupValue}"});
                        }
                    }

                    if (tcpDatagram.DestinationPort == 443)
                    {
                        if (packet.Length < 60) return;
                        if (packet.Buffer[54] == 0x16 && packet.Buffer[59] == 0x01)
                        {
                            var decodedPayload = tcpDatagram.Payload.Decode(Encoding.ASCII);
                            var filterMatch = filterPattern.Match(decodedPayload);
                            if (filterMatch.Success)
                                handle(new MonitorEvent {Type = typeof(WebRequestMonitor), Details = $"Restricted hostname identified during client hello SSL handshake matching the pattern: {filterPattern}"});
                        }
                    }
                }
            }
            catch (Exception e)
            {
                InterceptorService.EventLog.WriteEntry("HandlePacketData() -- " + e.Message, EventLogEntryType.Error);
            }
        }

        public void Start()
        {
            if (isSniffing) return;

            isSniffing = true;
            foreach (var task in communicators.Keys)
                task.Start();

            InterceptorService.EventLog.WriteEntry($"Monitoring for requests matching the pattern '{filterPattern}' on all live network adapters.");
        }

        public void Stop()
        {
            if (!isSniffing) return;

            isSniffing = false;
            foreach (var communicator in communicators.Values)
                communicator.Break();

            InterceptorService.EventLog.WriteEntry($"Stopped monitoring for '{filterPattern}' requests on all live network adapters.");
        }
    }
}