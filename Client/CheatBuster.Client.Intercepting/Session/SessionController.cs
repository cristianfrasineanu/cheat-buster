﻿using System;
using System.Diagnostics;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using CheatBuster.Client.Intercepting.Monitoring.Model;
using CheatBuster.Client.Intercepting.Transport;
using CheatBuster.Common.Models;
using CheatBuster.Common.Models.WebSocket.Messages;
using Newtonsoft.Json;

namespace CheatBuster.Client.Intercepting.Session
{
    internal class SessionController
    {
        private Session session;

        private readonly Connection connection;
        private readonly LockdownManager lockdownManager;

        public SessionController(Connection connection)
        {
            this.connection = connection;

            lockdownManager = new LockdownManager();
            lockdownManager.Locked += OnLocked;
            lockdownManager.Unlocked += OnUnlocked;
        }

        public void InitializeSession()
        {
            if (session != null) return;

            session = new Session();
            session.RefreshJob += OnRefreshJob;
            session.StartMonitoring += OnStartMonitoring;
            session.StopMonitoring += OnStopMonitoring;
            session.LaunchFullLock += OnLaunchFullLock;
            session.PublishScreenshot += OnPublishScreenshot;
        }

        public void DestroySession()
        {
            session?.Stop();
            session = null;
        }

        public async Task HandleAuthenticated()
        {
            if (session == null)
            {
                InterceptorService.EventLog.WriteEntry("InitializeActiveSession() -- Missing session.", EventLogEntryType.Error);
                return;
            }

            var monitoringJob = await LoadMonitoringJob();
            session.UseMonitoringJob(monitoringJob);
            session.CreateJobStatusTimer();
            session.CreateJobRefreshTimer();
            session.Start();
        }

        public void HandleDisconnected() => session?.Stop();

        public void HandleMessageReceived(Message<string> message)
        {
            if (message.Action == ClientMessages.Info)
            {
                InterceptorService.EventLog.WriteEntry($"(info) {message.Data}", EventLogEntryType.Information);
                return;
            }

            if (message.Action == ClientMessages.Locked)
            {
                lockdownManager.ExecuteFullClientLock();
                InterceptorService.EventLog.WriteEntry("The supervisor initiated manual client lockdown.", EventLogEntryType.Warning);
            }
            else if (message.Action == ClientMessages.Unlocked)
            {
                lockdownManager.DisableClientLock();
                InterceptorService.EventLog.WriteEntry("The supervisor manually unlocked the client.", EventLogEntryType.Warning);
            }
            else
                InterceptorService.EventLog.WriteEntry($"HandleMessageReceived() -- Unsupported client action {message.Action}", EventLogEntryType.Error);
        }

        private void OnRefreshJob()
        {
            var task = LoadMonitoringJob();
            LoadMonitoringJob().Wait();
            var monitoringJob = task.Result;

            session.UseMonitoringJob(monitoringJob);
            session.CreateJobRefreshTimer();
        }

        private async Task<MonitoringJob> LoadMonitoringJob()
        {
            var response = await connection.SendGet(SessionRoutes.Actions[SessionActions.LoadMonitoringJob]);
            if (!response.IsSuccessStatusCode)
            {
                InterceptorService.EventLog.WriteEntry($"LoadMonitoringJob() -- The request failed with status {response.StatusCode}", EventLogEntryType.Error);
                return null;
            }

            var content = await response.Content.ReadAsStringAsync();
            var monitoringJob = JsonConvert.DeserializeObject<MonitoringJob>(content);
            return monitoringJob;
        }

        private void OnStartMonitoring() => connection.SendMessage(ClientMessageFormatter.GenerateActivatedMessage("")).Wait();

        private void OnStopMonitoring() => connection.SendMessage(ClientMessageFormatter.GenerateDeactivatedMessage("")).Wait();

        private void OnLaunchFullLock() => lockdownManager.ExecuteFullClientLock();

        private void OnPublishScreenshot(Image screenshot)
        {
            try
            {
                var task = connection.SendPost(SessionRoutes.Actions[SessionActions.PublishScreenshot], screenshot, new BsonMediaTypeFormatter());
                task.Wait();
                if (!task.Result.IsSuccessStatusCode)
                    InterceptorService.EventLog.WriteEntry("OnPublishScreenshot() -- Failed to publish screenshot to server.", EventLogEntryType.Error);
            }
            catch (Exception e)
            {
                InterceptorService.EventLog.WriteEntry($"OnPublishScreenshot() -- Unexpected error when sending the request: {e.Message}", EventLogEntryType.Error);
            }
        }

        private void OnLocked() => connection.SendMessage(ClientMessageFormatter.GenerateLockedMessage("")).Wait();

        private void OnUnlocked() => connection.SendMessage(ClientMessageFormatter.GenerateUnlockedMessage("")).Wait();
    }
}