﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Timers;
using CheatBuster.Client.Intercepting.Monitoring.Model;
using CheatBuster.Client.Intercepting.Monitoring.Monitors;
using CheatBuster.Client.Intercepting.Monitoring.Monitors.Network;
using CheatBuster.Client.Intercepting.Monitoring.Monitors.System.Event;
using CheatBuster.Common.Models;
using CheatBuster.Common.Models.Monitoring;
using Timer = System.Timers.Timer;

namespace CheatBuster.Client.Intercepting.Session
{
    internal class Session
    {
        private static readonly double ActiveCheckInterval = TimeSpan.FromSeconds(10).TotalMilliseconds;

        public Guid Id { get; }

        private readonly ICollection<IMonitor> blacklistMonitors = new List<IMonitor>();
        private MonitoringJob monitoringJob;
        private bool isMonitoring;

        private Timer jobStatusTimer;
        private Timer jobRefreshTimer;

        #region Events

        public event Action RefreshJob;
        public event Action StartMonitoring;
        public event Action StopMonitoring;
        public event Action LaunchFullLock;
        public event Action<Image> PublishScreenshot;

        protected virtual void OnRefreshJob()
        {
            RefreshJob?.Invoke();
        }

        protected virtual void OnStartMonitoring()
        {
            if (blacklistMonitors.Count == 0)
            {
                InterceptorService.EventLog.WriteEntry("OnStartMonitoring() -- Unexpected empty blacklist monitors collection.", EventLogEntryType.Error);
                return;
            }

            isMonitoring = true;

            foreach (var monitor in blacklistMonitors)
                monitor.Start();
            StartMonitoring?.Invoke();
        }

        protected virtual void OnStopMonitoring()
        {
            isMonitoring = false;

            foreach (var monitor in blacklistMonitors)
                monitor.Stop();

            StopMonitoring?.Invoke();
        }

        protected virtual void OnLaunchFullLock()
        {
            LaunchFullLock?.Invoke();
        }

        protected virtual void OnPublishScreenshot(Image screenshot)
        {
            PublishScreenshot?.Invoke(screenshot);
        }

        #endregion

        public Session() => Id = Guid.NewGuid();

        public void UseMonitoringJob(MonitoringJob job)
        {
            if (monitoringJob?.LastModified != null &&
                monitoringJob.LastModified.Value >= job.LastModified) return;

            monitoringJob = job;
            InterceptorService.EventLog.WriteEntry($"Loaded new {job.AccordingCluster} monitoring job.", EventLogEntryType.Information);

            RefreshTimers();
            RefreshMonitors();
        }

        public void CreateJobStatusTimer()
        {
            if (jobStatusTimer != null) return;

            jobStatusTimer = new Timer
            {
                Interval = ActiveCheckInterval,
                AutoReset = true
            };
            jobStatusTimer.Elapsed += HandleJobStatusCheck;
        }

        public void CreateJobRefreshTimer()
        {
            if (monitoringJob == null)
            {
                InterceptorService.EventLog.WriteEntry("CreateJobRefreshTimer() -- Unexpected null monitoring job.", EventLogEntryType.Error);
                return;
            }

            var pollingInterval = monitoringJob.PollingRate;
            if (jobRefreshTimer != null || pollingInterval == TimeSpan.Zero) return;

            jobRefreshTimer = new Timer
            {
                Interval = pollingInterval.TotalMilliseconds,
                AutoReset = true
            };
            jobRefreshTimer.Elapsed += HandleJobRefresh;
        }

        public void Start()
        {
            if (monitoringJob == null)
            {
                InterceptorService.EventLog.WriteEntry("Start() -- Unexpected null monitoring job.", EventLogEntryType.Error);
                return;
            }

            InterceptorService.EventLog.WriteEntry($"Session with ID {Id} was started.", EventLogEntryType.Information);
            jobStatusTimer?.Start();
            jobRefreshTimer?.Start();
            if (blacklistMonitors.Count == 0) RefreshMonitors();
        }

        public void Stop()
        {
            InterceptorService.EventLog.WriteEntry($"Session with ID {Id} was stopped.", EventLogEntryType.Information);

            jobStatusTimer?.Stop();
            jobRefreshTimer?.Stop();
            OnStopMonitoring();
        }

        private void RefreshTimers()
        {
            if (jobRefreshTimer == null) return;
            if (TimeSpan.Compare(TimeSpan.FromMilliseconds(jobRefreshTimer.Interval), monitoringJob.PollingRate) == 0) return;

            jobRefreshTimer.Interval = monitoringJob.PollingRate.TotalMilliseconds;
            InterceptorService.EventLog.WriteEntry($"Changed the job {Id} refresh rate to {monitoringJob.PollingRate.TotalSeconds} seconds.", EventLogEntryType.Information);
        }

        private void RefreshMonitors()
        {
            InterceptorService.EventLog.WriteEntry($"Refreshing the event monitors for session {Id}", EventLogEntryType.Information);

            ClearMonitors();
            foreach (var rule in monitoringJob.Rules)
            {
                var filterMonitors = RecognizedFilters.Actions[rule.Action];
                if (filterMonitors == null || !filterMonitors.ContainsKey(rule.Filter) && rule.Action != ActionType.WebRequest)
                {
                    InterceptorService.EventLog.WriteEntry($"Unsupported filter: {rule}", EventLogEntryType.Warning);
                    continue;
                }

                IMonitor monitor;
                if (rule.Action == ActionType.WebRequest)
                    monitor = new WebRequestMonitor(HandleMonitorEvent, rule.Filter);
                else
                    monitor = Activator.CreateInstance(filterMonitors[rule.Filter], new MonitorEventHandler(HandleMonitorEvent)) as IMonitor;
                if (monitor == null)
                {
                    InterceptorService.EventLog.WriteEntry($"Unexpected error when activating the {rule} monitor.", EventLogEntryType.Error);
                    continue;
                }

                if (rule.Type == RuleType.Whitelist)
                {
                    // TODO: Whitelist monitors.
                }
                else
                {
                    blacklistMonitors.Add(monitor);
                    if (isMonitoring)
                        monitor.Start();
                }
            }
        }

        private void HandleMonitorEvent(MonitorEvent monitorEvent)
        {
            if (monitorEvent.Type == typeof(DesktopActivityMonitor))
                HandleDesktopActivityMonitorEvent(monitorEvent.Details);
            else
            {
                InterceptorService.EventLog.WriteEntry(monitorEvent.Details, EventLogEntryType.Warning);
                OnLaunchFullLock();
            }

            // TODO: Accumulate incidents.
        }

        private void HandleDesktopActivityMonitorEvent(string path)
        {
            var fileInfo = new FileInfo(path);
            if (!fileInfo.Exists)
            {
                InterceptorService.EventLog.WriteEntry("HandleMonitorEvent() -- The screenshot file does not exist.", EventLogEntryType.Error);
                return;
            }

            if (fileInfo.Extension != ".jpeg")
            {
                InterceptorService.EventLog.WriteEntry("HandleMonitorEvent() -- Unsupported screenshot file format.", EventLogEntryType.Error);
                return;
            }

            byte[] data;
            try
            {
                data = new byte[fileInfo.Length];
                using (var fs = fileInfo.OpenRead())
                    fs.Read(data, 0, data.Length);
            }
            catch (Exception e)
            {
                InterceptorService.EventLog.WriteEntry($"HandleDesktopActivityMonitorEvent() -- {e.Message}", EventLogEntryType.Error);
                return;
            }

            var image = new Image
            {
                FileName = fileInfo.Name,
                MimeType = "image/jpeg",
                Data = data
            };
            OnPublishScreenshot(image);
        }

        private void HandleJobRefresh(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            OnRefreshJob();
        }

        private void HandleJobStatusCheck(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            if (!ShouldMonitor() && isMonitoring)
            {
                InterceptorService.EventLog.WriteEntry($"Monitoring disabled for session with ID {Id}.", EventLogEntryType.Information);
                OnStopMonitoring();
            }
            else if (ShouldMonitor() && !isMonitoring)
            {
                InterceptorService.EventLog.WriteEntry($"Monitoring enabled for session with ID {Id}.", EventLogEntryType.Information);
                OnStartMonitoring();
            }
        }

        private bool ShouldMonitor()
        {
            var now = DateTime.Now;
            return now >= monitoringJob.StartAt && DateTime.Now <= monitoringJob.EndAt;
        }

        private void ClearMonitors()
        {
            if (blacklistMonitors.Count == 0) return;
            foreach (var monitor in blacklistMonitors.ToList())
            {
                monitor.Stop();
                blacklistMonitors.Remove(monitor);
            }
        }
    }
}