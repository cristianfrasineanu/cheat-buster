﻿using System;
using System.Diagnostics;
using System.IO;
using CheatBuster.Client.Intercepting.Utils;
using CheatBuster.Client.UserInterfaceLocking;

namespace CheatBuster.Client.Intercepting.Session
{
    internal class LockdownManager
    {
        private static readonly string LockdownAssemblyLocation = typeof(InterfaceManager).Assembly.Location;
        private static readonly string LockdownAssemblyName = typeof(InterfaceManager).Assembly.GetName().Name;

        private readonly string lockdownExecutablePath = Path.GetFullPath(LockdownAssemblyLocation);
        public bool IsLocked { get; private set; }

        public event Action Locked;
        public event Action Unlocked;

        protected virtual void OnLocked()
        {
            IsLocked = true;
            Locked?.Invoke();
        }

        protected virtual void OnUnlocked()
        {
            IsLocked = false;
            Unlocked?.Invoke();
        }

        // TODO: Store process PID so it can be easily killed later.
        public void ExecuteFullClientLock()
        {
            if (IsLocked) return;

            try
            {
                NativeProcessManager.StartProcessAsCurrentUser(lockdownExecutablePath);
            }
            catch (Exception e)
            {
                InterceptorService.EventLog.WriteEntry($"ExecuteFullClientLock() -- {e.Message}", EventLogEntryType.Error);
                return;
            }

            OnLocked();
            InterceptorService.EventLog.WriteEntry("Full lock successfully started.", EventLogEntryType.Information);
        }

        public void DisableClientLock()
        {
            if (!IsLocked) return;

            try
            {
                var processesByName = Process.GetProcessesByName(LockdownAssemblyName);
                if (processesByName.Length == 0)
                {
                    InterceptorService.EventLog.WriteEntry($"The client lock process was already terminated.", EventLogEntryType.Warning);
                    OnUnlocked();
                    return;
                }

                processesByName[0].Kill();
            }
            catch (Exception e)
            {
                InterceptorService.EventLog.WriteEntry($"DisableClientLock() -- {e.Message}", EventLogEntryType.Error);
                return;
            }

            OnUnlocked();
            InterceptorService.EventLog.WriteEntry("Client lock successfully disabled.", EventLogEntryType.Information);
        }
    }
}