﻿using System.Collections.Generic;

namespace CheatBuster.Client.Intercepting.Session
{
    internal class SessionRoutes
    {
        public static Dictionary<SessionActions, string> Actions = new Dictionary<SessionActions, string>
        {
            {SessionActions.LoadMonitoringJob, "/client/job"},
            {SessionActions.PublishScreenshot, "/client/screenshot"}
        };
    }
}