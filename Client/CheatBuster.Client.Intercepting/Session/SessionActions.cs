﻿namespace CheatBuster.Client.Intercepting.Session
{
    internal enum SessionActions
    {
        LoadMonitoringJob,
        PublishScreenshot,
        SendIncident
    }
}