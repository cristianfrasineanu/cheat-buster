﻿using System;

namespace CheatBuster.Client.UserInterfaceLocking.Tools
{
    internal class Bootstrap
    {
        // TODO: Make the process configurable via flags
        [STAThread]
        private static void Main(string[] args)
        {
            InterfaceManager.StartThreadedLockdown();
            // TODO: Hidden shortcut for disabling the lock.
        }
    }
}