﻿using System.Threading;
using System.Windows;
using CheatBuster.Client.UserInterfaceLocking.Hooks.Keyboard;
using CheatBuster.Client.UserInterfaceLocking.Presentation;

namespace CheatBuster.Client.UserInterfaceLocking
{
    public class InterfaceManager : Application
    {
        private readonly KeyboardDisabler keyboardDisabler;

        public InterfaceManager()
        {
            keyboardDisabler = new KeyboardDisabler();
        }

        public static void StartThreadedLockdown()
        {
            var lockdownApplicationThread = new Thread(() =>
            {
                var lockdownApplication = new InterfaceManager();
                var window = new OverlayWindow();
                lockdownApplication.Run(window);
            });
            lockdownApplicationThread.SetApartmentState(ApartmentState.STA);
            lockdownApplicationThread.Start();
        }

        public static void ShutdownViaDispatcher()
        {
            Current.Dispatcher.InvokeShutdown();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            keyboardDisabler.HookKeyboard();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            keyboardDisabler.UnhookKeyboard();
        }
    }
}