﻿using System.Runtime.InteropServices;

namespace CheatBuster.Client.UserInterfaceLocking.Presentation
{
    public partial class OverlayWindow
    {
        [DllImport("User32.dll")]
        private static extern int ShowCursor(bool show);

        public OverlayWindow()
        {
            InitializeComponent();
            ShowCursor(false);
        }
    }
}