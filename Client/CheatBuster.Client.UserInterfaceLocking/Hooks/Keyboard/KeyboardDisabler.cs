﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CheatBuster.Client.UserInterfaceLocking.Hooks.Keyboard
{
    public class KeyboardDisabler
    {
        private static IntPtr _keyboardHook = IntPtr.Zero;
        private bool isHookInstalled;

        private static readonly IList<Keys> IllegalKeys = new List<Keys>(new[] {Keys.LWin, Keys.RWin, Keys.LControlKey, Keys.RControlKey, Keys.Escape});

        public void HookKeyboard()
        {
            if (isHookInstalled)
                throw new Exception("Keyboard hook already installed.");

            var currentProcess = Process.GetCurrentProcess().MainModule;
            _keyboardHook = NativeKeyboardHook.SetKeyboardHook(BlockExitCombinations, currentProcess.ModuleName);
            isHookInstalled = _keyboardHook != IntPtr.Zero;
        }

        private IntPtr BlockExitCombinations(int code, IntPtr wParam, IntPtr lParam)
        {
            if (code < 0)
                return NativeKeyboardHook.CallNextHookEx(_keyboardHook, code, wParam, lParam);

            var keyboardHookInput = (NativeKeyboardHook.KeyboardHookInput) Marshal.PtrToStructure(lParam, typeof(NativeKeyboardHook.KeyboardHookInput));
            if (IllegalKeys.Contains(keyboardHookInput.key) || NativeKeyboardHook.WasAltModifierPressed(keyboardHookInput.flags))
                return (IntPtr) 1;

            return NativeKeyboardHook.CallNextHookEx(_keyboardHook, code, wParam, lParam);
        }

        public void UnhookKeyboard()
        {
            if (!isHookInstalled)
                throw new Exception("The keyboard hook must be installed first.");

            if (!NativeKeyboardHook.UnhookWindowsHookEx(_keyboardHook))
                throw new Exception("UnhookWindowsHookEx failed: -" + Marshal.GetLastWin32Error());
        }
    }
}