﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CheatBuster.Client.UserInterfaceLocking.Hooks.Keyboard
{
    public static class NativeKeyboardHook
    {
        #region Constants

        private const int LowLevelKeyboardHookId = 13;

        #endregion

        #region DllImports

        [DllImport("User32.dll", SetLastError = true)]
        private static extern IntPtr
            SetWindowsHookEx(int hookId, HookHandler callback, IntPtr hookModule, uint threadId);

        [DllImport("User32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool UnhookWindowsHookEx(IntPtr hook);

        [DllImport("User32.dll")]
        public static extern IntPtr CallNextHookEx(IntPtr hook, int code, IntPtr wParam, IntPtr lParam);

        [DllImport("Kernel32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr GetModuleHandle(string name);

        #endregion

        #region Delegates

        public delegate IntPtr HookHandler(int code, IntPtr wParam, IntPtr lParam);

        // Prevent CallbackOnCollectedDelegate exception
        private static HookHandler _keyboardHandler;

        #endregion

        #region Structs

        [StructLayout(LayoutKind.Sequential)]
        public struct KeyboardHookInput
        {
            public readonly Keys key;
            public readonly int scanCode;
            public readonly int flags;
            public readonly int time;
            public readonly IntPtr extra;
        }

        #endregion

        public static bool WasAltModifierPressed(int flags)
        {
            return (flags & (1 << 5)) == 1 << 5;
        }

        public static IntPtr SetKeyboardHook(HookHandler handler, string moduleName)
        {
            _keyboardHandler = handler;
            return SetWindowsHookEx(LowLevelKeyboardHookId, _keyboardHandler, GetModuleHandle(moduleName), 0);
        }
    }
}