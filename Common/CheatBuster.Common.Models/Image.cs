﻿namespace CheatBuster.Common.Models
{
    public class Image
    {
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public byte[] Data { get; set; }
    }
}