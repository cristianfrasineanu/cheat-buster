﻿namespace CheatBuster.Common.Models.WebSocket.Messages
{
    public class Message<T>
    {
        public string Action { get; set; }
        public T Data { get; set; }
    }
}