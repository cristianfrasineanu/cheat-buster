﻿namespace CheatBuster.Common.Models.WebSocket.Messages
{
    public class AdminMessageFormatter : MessageFormatter
    {
        public static string GenerateInformationMessage<T>(T data)
        {
            return GenerateMessage(AdminMessages.Info, data);
        }

        public static string GenerateAddMessage<T>(T data)
        {
            return GenerateMessage(AdminMessages.Add, data);
        }

        public static string GenerateUpdateMessage<T>(T data)
        {
            return GenerateMessage(AdminMessages.Update, data);
        }

        public static string GenerateDeleteMessage<T>(T data)
        {
            return GenerateMessage(AdminMessages.Delete, data);
        }
    }
}
