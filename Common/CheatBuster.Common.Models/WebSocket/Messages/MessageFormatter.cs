﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CheatBuster.Common.Models.WebSocket.Messages
{
    public abstract class MessageFormatter
    {
        protected static string GenerateMessage<T>(string type, T data)
        {
            return JsonConvert.SerializeObject(
                new Message<T> {Action = type, Data = data},
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }
            );
        }

        public static Message<T> DeserializeMessage<T>(string data)
        {
            return JsonConvert.DeserializeObject<Message<T>>(
                data,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }
            );
        }

        public static T Deserialize<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(
                data,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }
            );
        }
    }
}