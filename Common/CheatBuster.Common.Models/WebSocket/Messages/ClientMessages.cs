﻿namespace CheatBuster.Common.Models.WebSocket.Messages
{
    public static class ClientMessages
    {
        public static string Info { get; } = "_info";
        public static string ServiceActivated { get; } = "_activated";
        public static string ServiceDeactivated { get; } = "_deactivated";
        public static string Locked { get; } = "_locked";
        public static string Unlocked { get; } = "_unlocked";
    }
}