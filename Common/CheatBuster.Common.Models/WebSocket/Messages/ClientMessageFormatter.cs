﻿namespace CheatBuster.Common.Models.WebSocket.Messages
{
    public class ClientMessageFormatter : MessageFormatter
    {
        public static string GenerateInformationMessage<T>(T data)
        {
            return GenerateMessage(ClientMessages.Info, data);
        }

        public static string GenerateActivatedMessage<T>(T data)
        {
            return GenerateMessage(ClientMessages.ServiceActivated, data);
        }

        public static string GenerateDeactivatedMessage<T>(T data)
        {
            return GenerateMessage(ClientMessages.ServiceDeactivated, data);
        }

        public static string GenerateLockedMessage<T>(T data)
        {
            return GenerateMessage(ClientMessages.Locked, data);
        }

        public static string GenerateUnlockedMessage<T>(T data)
        {
            return GenerateMessage(ClientMessages.Unlocked, data);
        }
    }
}