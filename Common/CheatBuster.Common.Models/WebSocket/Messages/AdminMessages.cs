﻿namespace CheatBuster.Common.Models.WebSocket.Messages
{
    public static class AdminMessages
    {
        public static string Info { get; } = "_info";
        public static string ManualLock { get; } = "_lock";
        public static string ManualUnlock { get; } = "_unlock";
        public static string Terminate { get; } = "_terminate";
        public static string Add { get; } = "_add";
        public static string Update { get; } = "_update";
        public static string Delete { get; } = "_delete";
    }
}