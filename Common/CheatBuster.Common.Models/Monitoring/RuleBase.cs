﻿using Newtonsoft.Json;

namespace CheatBuster.Common.Models.Monitoring
{
    public class RuleBase
    {
        [JsonProperty("type")] public RuleType Type { get; set; }
        [JsonProperty("action")] public ActionType Action { get; set; }
        [JsonProperty("filter")] public string Filter { get; set; }
    }
}