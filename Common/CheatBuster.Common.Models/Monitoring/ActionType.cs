﻿namespace CheatBuster.Common.Models.Monitoring
{
    public enum ActionType
    {
        WebRequest,
        ProtocolUsage,
        SystemEvent,
        SystemProcess
    }
}