﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace CheatBuster.Common.Models.Monitoring
{
    public class MonitoringJobBase<T> where T : RuleBase
    {
        [JsonProperty("accordingCluster")] public virtual string AccordingCluster { get; set; }
        [JsonProperty("rules")] public virtual ICollection<T> Rules { get; set; }
        [JsonProperty("startAt")] public DateTime StartAt { get; set; }
        [JsonProperty("endAt")] public DateTime EndAt { get; set; }

        [JsonProperty("pollingRate")] public TimeSpan PollingRate { get; set; }

        // A null push rate indicates that the client runs offline without sending data to the server.
        [JsonProperty("pushRate")] public TimeSpan? PushRate { get; set; }
        [JsonProperty("lastModified")] public DateTime? LastModified { get; set; }
    }
}