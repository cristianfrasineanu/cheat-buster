﻿namespace CheatBuster.Common.Models.Monitoring
{
    public enum RuleType
    {
        Whitelist,
        Blacklist
    }
}