﻿namespace CheatBuster.Common.Models
{
    public class MachineAuthentication
    {
        public string MachineName { get; set; }
        public string Password { get; set; }
    }
}